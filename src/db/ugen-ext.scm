(define a2b
  (lambda (rt a b c d)
    (construct-ugen "A2B" rt (list a b c d) nil 4 nil)))

(define ay
  (lambda (rt tonea toneb tonec noise control_ vola volb volc envfreq envstyle chiptype)
    (construct-ugen "AY" rt (list tonea toneb tonec noise control_ vola volb volc envfreq envstyle chiptype) nil 1 nil)))

(define ay8910
  (lambda (rt r0 r1 r2 r3 r4 r5 r6 r7 r8 r9 rA rB rC rD rate_)
    (construct-ugen "AY8910" rt (list r0 r1 r2 r3 r4 r5 r6 r7 r8 r9 rA rB rC rD rate_) nil 3 nil)))

(define allpass1
  (lambda (rt input freq)
    (construct-ugen "Allpass1" rt (list input freq) nil 1 nil)))

(define allpass2
  (lambda (rt input freq rq)
    (construct-ugen "Allpass2" rt (list input freq rq) nil 1 nil)))

(define amplitudemod
  (lambda (rt input attackTime releaseTime)
    (construct-ugen "AmplitudeMod" rt (list input attackTime releaseTime) nil 1 nil)))

(define analogbassdrum
  (lambda (rt trig_ infsustain accent freq tone decay_ attackfm selffm)
    (construct-ugen "AnalogBassDrum" rt (list trig_ infsustain accent freq tone decay_ attackfm selffm) nil 1 nil)))

(define analogfoldosc
  (lambda (rt freq amp)
    (construct-ugen "AnalogFoldOsc" rt (list freq amp) nil 1 nil)))

(define analogphaser
  (lambda (input lfoinput skew feedback modulation stages)
    (construct-ugen "AnalogPhaser" (list 0) (list input lfoinput skew feedback modulation stages) nil 1 nil)))

(define analogphasermod
  (lambda (input skew modulation stages)
    (construct-ugen "AnalogPhaserMod" (list 0) (list input skew modulation stages) nil 1 nil)))

(define analogpulseshaper
  (lambda (rt pulseinput width decay_ double)
    (construct-ugen "AnalogPulseShaper" rt (list pulseinput width decay_ double) nil 1 nil)))

(define analogsnaredrum
  (lambda (rt trig_ infsustain accent freq tone decay_ snappy)
    (construct-ugen "AnalogSnareDrum" rt (list trig_ infsustain accent freq tone decay_ snappy) nil 1 nil)))

(define analogtape
  (lambda (input bias saturation drive oversample mode)
    (construct-ugen "AnalogTape" (list 0) (list input bias saturation drive oversample mode) nil 1 nil)))

(define analogvintagedistortion
  (lambda (input drivegain bias lowgain highgain shelvingfreq oversample)
    (construct-ugen "AnalogVintageDistortion" (list 0) (list input drivegain bias lowgain highgain shelvingfreq oversample) nil 1 nil)))

(define analyseevents2
  (lambda (rt input bufnum threshold triggerid circular pitch)
    (construct-ugen "AnalyseEvents2" rt (list input bufnum threshold triggerid circular pitch) nil 1 nil)))

(define arneodocoullettresser
  (lambda (rt freq alpha h xi yi zi)
    (construct-ugen "ArneodoCoulletTresser" rt (list freq alpha h xi yi zi) nil 3 nil)))

(define arraymax
  (lambda (array)
    (construct-ugen "ArrayMax" (list 0) nil array 2 nil)))

(define arraymin
  (lambda (array)
    (construct-ugen "ArrayMin" (list 0) nil array 2 nil)))

(define astrocade
  (lambda (rt reg0 reg1 reg2 reg3 reg4 reg5 reg6 reg7)
    (construct-ugen "Astrocade" rt (list reg0 reg1 reg2 reg3 reg4 reg5 reg6 reg7) nil 1 nil)))

(define atari2600
  (lambda (rt audc0 audc1 audf0 audf1 audv0 audv1 rate_)
    (construct-ugen "Atari2600" rt (list audc0 audc1 audf0 audf1 audv0 audv1 rate_) nil 1 nil)))

(define atsamp
  (lambda (rt atsbuffer partialNum filePointer)
    (construct-ugen "AtsAmp" rt (list atsbuffer partialNum filePointer) nil 1 nil)))

(define atsband
  (lambda (rt atsbuffer band filePointer)
    (construct-ugen "AtsBand" rt (list atsbuffer band filePointer) nil 1 nil)))

(define atsfreq
  (lambda (rt atsbuffer partialNum filePointer)
    (construct-ugen "AtsFreq" rt (list atsbuffer partialNum filePointer) nil 1 nil)))

(define atsnoisynth
  (lambda (rt atsbuffer numPartials partialStart partialSkip filePointer sinePct noisePct freqMul freqAdd numBands bandStart bandSkip)
    (construct-ugen "AtsNoiSynth" rt (list atsbuffer numPartials partialStart partialSkip filePointer sinePct noisePct freqMul freqAdd numBands bandStart bandSkip) nil 1 nil)))

(define atsnoise
  (lambda (rt atsbuffer bandNum filePointer)
    (construct-ugen "AtsNoise" rt (list atsbuffer bandNum filePointer) nil 1 nil)))

(define atsparinfo
  (lambda (rt atsbuffer partialNum filePointer)
    (construct-ugen "AtsParInfo" rt (list atsbuffer partialNum filePointer) nil 2 nil)))

(define atspartial
  (lambda (rt atsbuffer partial filePointer freqMul freqAdd)
    (construct-ugen "AtsPartial" rt (list atsbuffer partial filePointer freqMul freqAdd) nil 1 nil)))

(define atssynth
  (lambda (rt atsbuffer numPartials partialStart partialSkip filePointer freqMul freqAdd)
    (construct-ugen "AtsSynth" rt (list atsbuffer numPartials partialStart partialSkip filePointer freqMul freqAdd) nil 1 nil)))

(define atsugen
  (lambda (rt maxSize)
    (construct-ugen "AtsUGen" rt (list maxSize) nil 1 nil)))

(define attackslope
  (lambda (rt input windowsize peakpicksize leak energythreshold sumthreshold mingap numslopesaveraged)
    (construct-ugen "AttackSlope" rt (list input windowsize peakpicksize leak energythreshold sumthreshold mingap numslopesaveraged) nil 6 nil)))

(define audiomsg
  (lambda (input index)
    (construct-ugen "AudioMSG" (list 0) (list input index) nil 1 nil)))

(define averageoutput
  (lambda (input trig_)
    (construct-ugen "AverageOutput" (list 0) (list input trig_) nil 1 nil)))

(define b2a
  (lambda (rt w x y z)
    (construct-ugen "B2A" rt (list w x y z) nil 4 nil)))

(define b2ster
  (lambda (rt w x y)
    (construct-ugen "B2Ster" rt (list w x y) nil 2 nil)))

(define b2uhj
  (lambda (rt w x y)
    (construct-ugen "B2UHJ" rt (list w x y) nil 2 nil)))

(define bblockerbuf
  (lambda (rt freq bufnum startpoint)
    (construct-ugen "BBlockerBuf" rt (list freq bufnum startpoint) nil 9 nil)))

(define bfdecode1
  (lambda (rt w x y z azimuth elevation wComp)
    (construct-ugen "BFDecode1" rt (list w x y z azimuth elevation wComp) nil 1 nil)))

(define bfdecoder
  (lambda (rt maxSize)
    (construct-ugen "BFDecoder" rt (list maxSize) nil 1 nil)))

(define bfencode1
  (lambda (rt input azimuth elevation rho gain wComp)
    (construct-ugen "BFEncode1" rt (list input azimuth elevation rho gain wComp) nil 4 nil)))

(define bfencode2
  (lambda (rt input point_x point_y elevation gain wComp)
    (construct-ugen "BFEncode2" rt (list input point_x point_y elevation gain wComp) nil 4 nil)))

(define bfencodester
  (lambda (rt l r azimuth width elevation rho gain wComp)
    (construct-ugen "BFEncodeSter" rt (list l r azimuth width elevation rho gain wComp) nil 4 nil)))

(define bfgrainpanner
  (lambda (rt maxSize)
    (construct-ugen "BFGrainPanner" rt (list maxSize) nil 1 nil)))

(define bfmanipulate
  (lambda (rt w x y z rotate tilt tumble)
    (construct-ugen "BFManipulate" rt (list w x y z rotate tilt tumble) nil 4 nil)))

(define bfpanner
  (lambda (rt maxSize)
    (construct-ugen "BFPanner" rt (list maxSize) nil 1 nil)))

(define blbufrd
  (lambda (rt bufnum phase ratio)
    (construct-ugen "BLBufRd" rt (list bufnum phase ratio) nil 1 nil)))

(define blosc
  (lambda (rt freq pulsewidth waveform)
    (construct-ugen "BLOsc" rt (list freq pulsewidth waveform) nil 1 nil)))

(define bmoog
  (lambda (input freq q mode saturation)
    (construct-ugen "BMoog" (list 0) (list input freq q mode saturation) nil 1 nil)))

(define balance
  (lambda (rt input test hp stor)
    (construct-ugen "Balance" rt (list input test hp stor) nil 1 nil)))

(define beatstatistics
  (lambda (rt fft leak numpreviousbeats)
    (construct-ugen "BeatStatistics" rt (list fft leak numpreviousbeats) nil 4 nil)))

(define beep
  (lambda (rt freq vol)
    (construct-ugen "Beep" rt (list freq vol) nil 1 nil)))

(define beepu
  (lambda (rt maxSize)
    (construct-ugen "BeepU" rt (list maxSize) nil 1 nil)))

(define bindata
  (lambda (rt buffer bin overlaps)
    (construct-ugen "BinData" rt (list buffer bin overlaps) nil 2 nil)))

(define blitb3
  (lambda (rt freq)
    (construct-ugen "BlitB3" rt (list freq) nil 1 nil)))

(define blitb3d
  (lambda (rt freq)
    (construct-ugen "BlitB3D" rt (list freq) nil 1 nil)))

(define blitb3saw
  (lambda (rt freq leak)
    (construct-ugen "BlitB3Saw" rt (list freq leak) nil 1 nil)))

(define blitb3square
  (lambda (rt freq leak)
    (construct-ugen "BlitB3Square" rt (list freq leak) nil 1 nil)))

(define blitb3tri
  (lambda (rt freq leak leak2)
    (construct-ugen "BlitB3Tri" rt (list freq leak leak2) nil 1 nil)))

(define breakcore
  (lambda (rt bufnum capturein capturetrigger duration ampdropout)
    (construct-ugen "Breakcore" rt (list bufnum capturein capturetrigger duration ampdropout) nil 1 nil)))

(define brusselator
  (lambda (rt reset rate_ mu gamma initx inity)
    (construct-ugen "Brusselator" rt (list reset rate_ mu gamma initx inity) nil 2 nil)))

(define bufgrain
  (lambda (rt trigger dur sndbuf rate_ pos interp)
    (construct-ugen "BufGrain" rt (list trigger dur sndbuf rate_ pos interp) nil 1 nil)))

(define bufgrainb
  (lambda (rt trigger dur sndbuf rate_ pos envbuf interp)
    (construct-ugen "BufGrainB" rt (list trigger dur sndbuf rate_ pos envbuf interp) nil 1 nil)))

(define bufgrainbbf
  (lambda (rt trigger dur sndbuf rate_ pos envbuf azimuth elevation rho interp wComp)
    (construct-ugen "BufGrainBBF" rt (list trigger dur sndbuf rate_ pos envbuf azimuth elevation rho interp wComp) nil 4 nil)))

(define bufgrainbf
  (lambda (rt trigger dur sndbuf rate_ pos azimuth elevation rho interp wComp)
    (construct-ugen "BufGrainBF" rt (list trigger dur sndbuf rate_ pos azimuth elevation rho interp wComp) nil 4 nil)))

(define bufgraini
  (lambda (rt trigger dur sndbuf rate_ pos envbuf1 envbuf2 ifac interp)
    (construct-ugen "BufGrainI" rt (list trigger dur sndbuf rate_ pos envbuf1 envbuf2 ifac interp) nil 1 nil)))

(define bufgrainibf
  (lambda (rt trigger dur sndbuf rate_ pos envbuf1 envbuf2 ifac azimuth elevation rho interp wComp)
    (construct-ugen "BufGrainIBF" rt (list trigger dur sndbuf rate_ pos envbuf1 envbuf2 ifac azimuth elevation rho interp wComp) nil 4 nil)))

(define bufmax
  (lambda (rt bufnum gate_)
    (construct-ugen "BufMax" rt (list bufnum gate_) nil 2 nil)))

(define bufmin
  (lambda (rt bufnum gate_)
    (construct-ugen "BufMin" rt (list bufnum gate_) nil 2 nil)))

(define cq_diff
  (lambda (rt in1 in2 databufnum)
    (construct-ugen "CQ_Diff" rt (list in1 in2 databufnum) nil 1 nil)))

(define cepstrum
  (lambda (rt cepbuf fftchain)
    (construct-ugen "Cepstrum" rt (list cepbuf fftchain) nil 1 nil)))

(define chen
  (lambda (rt speed a b c)
    (construct-ugen "Chen" rt (list speed a b c) nil 3 nil)))

(define chromagram
  (lambda (rt fft fftsize n tuningbase octaves integrationflag coeff octaveratio perframenormalize)
    (construct-ugen "Chromagram" rt (list fft fftsize n tuningbase octaves integrationflag coeff octaveratio perframenormalize) nil 12 nil)))

(define circleramp
  (lambda (rt input lagTime circmin circmax)
    (construct-ugen "CircleRamp" rt (list input lagTime circmin circmax) nil 1 nil)))

(define clipper32
  (lambda (rt input lo hi)
    (construct-ugen "Clipper32" rt (list input lo hi) nil 1 nil)))

(define clipper4
  (lambda (rt input lo hi)
    (construct-ugen "Clipper4" rt (list input lo hi) nil 1 nil)))

(define clipper8
  (lambda (rt input lo hi)
    (construct-ugen "Clipper8" rt (list input lo hi) nil 1 nil)))

(define clockmus
  (lambda (rt)
    (construct-ugen "Clockmus" rt nil nil 1 nil)))

(define comblp
  (lambda (rt input gate_ maxdelaytime delaytime decaytime coef)
    (construct-ugen "CombLP" rt (list input gate_ maxdelaytime delaytime decaytime coef) nil 1 nil)))

(define complexres
  (lambda (input freq decay_)
    (construct-ugen "ComplexRes" (list 0) (list input freq decay_) nil 1 nil)))

(define u:concat
  (lambda (rt control_ source storesize seektime seekdur matchlength freezestore zcr lms sc st randscore)
    (construct-ugen "Concat" rt (list control_ source storesize seektime seekdur matchlength freezestore zcr lms sc st randscore) nil 1 nil)))

(define concat2
  (lambda (rt control_ source storesize seektime seekdur matchlength freezestore zcr lms sc st randscore threshold)
    (construct-ugen "Concat2" rt (list control_ source storesize seektime seekdur matchlength freezestore zcr lms sc st randscore threshold) nil 1 nil)))

(define coyote
  (lambda (rt input trackFall slowLag fastLag fastMul thresh minDur)
    (construct-ugen "Coyote" rt (list input trackFall slowLag fastLag fastMul thresh minDur) nil 1 nil)))

(define crest
  (lambda (rt input numsamps gate_)
    (construct-ugen "Crest" rt (list input numsamps gate_) nil 1 nil)))

(define crossoverdistortion
  (lambda (input amp smooth)
    (construct-ugen "CrossoverDistortion" (list 0) (list input amp smooth) nil 1 nil)))

(define dcompressor
  (lambda (input sidechainIn sidechain ratio threshold attack release makeup automakeup)
    (construct-ugen "DCompressor" (list 0) (list input sidechainIn sidechain ratio threshold attack release makeup automakeup) nil 1 nil)))

(define dfm1
  (lambda (input freq res inputgain type_ noiselevel)
    (construct-ugen "DFM1" (list 0) (list input freq res inputgain type_ noiselevel) nil 1 nil)))

(define dnoisering
  (lambda (change chance shift numBits resetval)
    (construct-ugen "DNoiseRing" demand-rate (list change chance shift numBits resetval) nil 1 nil)))

(define dpw3tri
  (lambda (rt freq)
    (construct-ugen "DPW3Tri" rt (list freq) nil 1 nil)))

(define dpw4saw
  (lambda (rt freq)
    (construct-ugen "DPW4Saw" rt (list freq) nil 1 nil)))

(define dwgbowed
  (lambda (rt freq velb force gate_ pos release c1 c3 impZ fB)
    (construct-ugen "DWGBowed" rt (list freq velb force gate_ pos release c1 c3 impZ fB) nil 1 nil)))

(define dwgbowedsimple
  (lambda (rt freq velb force gate_ pos release c1 c3)
    (construct-ugen "DWGBowedSimple" rt (list freq velb force gate_ pos release c1 c3) nil 1 nil)))

(define dwgbowedtor
  (lambda (rt freq velb force gate_ pos release c1 c3 impZ fB mistune c1tor c3tor iZtor)
    (construct-ugen "DWGBowedTor" rt (list freq velb force gate_ pos release c1 c3 impZ fB mistune c1tor c3tor iZtor) nil 1 nil)))

(define dwgplucked
  (lambda (rt freq amp gate_ pos c1 c3 inp release)
    (construct-ugen "DWGPlucked" rt (list freq amp gate_ pos c1 c3 inp release) nil 1 nil)))

(define dwgplucked2
  (lambda (rt freq amp gate_ pos c1 c3 inp release mistune mp gc)
    (construct-ugen "DWGPlucked2" rt (list freq amp gate_ pos c1 c3 inp release mistune mp gc) nil 1 nil)))

(define dwgpluckedstiff
  (lambda (rt freq amp gate_ pos c1 c3 inp release fB)
    (construct-ugen "DWGPluckedStiff" rt (list freq amp gate_ pos c1 c3 inp release fB) nil 1 nil)))

(define dwgsoundboard
  (lambda (inp c1 c3 mix d1 d2 d3 d4 d5 d6 d7 d8)
    (construct-ugen "DWGSoundBoard" (list 0) (list inp c1 c3 mix d1 d2 d3 d4 d5 d6 d7 d8) nil 1 nil)))

(define dbrown2
  (lambda (rt lo hi step dist length_)
    (construct-ugen "Dbrown2" rt (list lo hi step dist length_) nil 1 nil)))

(define dbuftag
  (lambda (bufnum v axiom rules recycle mode)
    (construct-ugen "DbufTag" demand-rate (list bufnum v axiom rules recycle mode) nil 1 nil)))

(define decimator
  (lambda (rt input rate_ bits)
    (construct-ugen "Decimator" rt (list input rate_ bits) nil 1 nil)))

(define detablockerbuf
  (lambda (bufnum startpoint)
    (construct-ugen "DetaBlockerBuf" demand-rate (list bufnum startpoint) nil 1 nil)))

(define dfsm
  (lambda (rules n rgen)
    (construct-ugen "Dfsm" demand-rate (list rules n rgen) nil 1 nil)))

(define dgauss
  (lambda (rt lo hi length_)
    (construct-ugen "Dgauss" rt (list lo hi length_) nil 1 nil)))

(define dioderingmod
  (lambda (car mod_)
    (construct-ugen "DiodeRingMod" (list 0) (list car mod_) nil 1 nil)))

(define disintegrator
  (lambda (input probability multiplier)
    (construct-ugen "Disintegrator" (list 0) (list input probability multiplier) nil 1 nil)))

(define dneuromodule
  (lambda (nc dt theta x weights)
    (construct-ugen "Dneuromodule" demand-rate (list dt) theta x weights nc nil)))

(define doublenestedallpassc
  (lambda (input maxdelay1 delay1 gain1 maxdelay2 delay2 gain2 maxdelay3 delay3 gain3)
    (construct-ugen "DoubleNestedAllpassC" (list 0) (list input maxdelay1 delay1 gain1 maxdelay2 delay2 gain2 maxdelay3 delay3 gain3) nil 1 nil)))

(define doublenestedallpassl
  (lambda (input maxdelay1 delay1 gain1 maxdelay2 delay2 gain2 maxdelay3 delay3 gain3)
    (construct-ugen "DoubleNestedAllpassL" (list 0) (list input maxdelay1 delay1 gain1 maxdelay2 delay2 gain2 maxdelay3 delay3 gain3) nil 1 nil)))

(define doublenestedallpassn
  (lambda (input maxdelay1 delay1 gain1 maxdelay2 delay2 gain2 maxdelay3 delay3 gain3)
    (construct-ugen "DoubleNestedAllpassN" (list 0) (list input maxdelay1 delay1 gain1 maxdelay2 delay2 gain2 maxdelay3 delay3 gain3) nil 1 nil)))

(define doublewell
  (lambda (rt reset ratex ratey f w delta initx inity)
    (construct-ugen "DoubleWell" rt (list reset ratex ratey f w delta initx inity) nil 1 nil)))

(define doublewell2
  (lambda (rt reset ratex ratey f w delta initx inity)
    (construct-ugen "DoubleWell2" rt (list reset ratex ratey f w delta initx inity) nil 1 nil)))

(define doublewell3
  (lambda (rt reset rate_ f delta initx inity)
    (construct-ugen "DoubleWell3" rt (list reset rate_ f delta initx inity) nil 1 nil)))

(define drivenoise
  (lambda (rt input amount multi)
    (construct-ugen "DriveNoise" rt (list input amount multi) nil 1 nil)))

(define drumtrack
  (lambda (rt input lock dynleak tempowt phasewt basswt patternwt prior kicksensitivity snaresensitivity debugmode)
    (construct-ugen "DrumTrack" rt (list input lock dynleak tempowt phasewt basswt patternwt prior kicksensitivity snaresensitivity debugmode) nil 4 nil)))

(define dtag
  (lambda (rt bufsize v axiom rules recycle mode)
    (construct-ugen "Dtag" rt (list bufsize v axiom rules recycle mode) nil 1 nil)))

(define envdetect
  (lambda (rt input attack release)
    (construct-ugen "EnvDetect" rt (list input attack release) nil 1 nil)))

(define envfollow
  (lambda (rt input decaycoeff)
    (construct-ugen "EnvFollow" rt (list input decaycoeff) nil 1 nil)))

(define fftcomplexdev
  (lambda (rt buffer rectify powthresh)
    (construct-ugen "FFTComplexDev" rt (list buffer rectify powthresh) nil 1 nil)))

(define fftcrest
  (lambda (rt buffer freqlo freqhi)
    (construct-ugen "FFTCrest" rt (list buffer freqlo freqhi) nil 1 nil)))

(define fftdiffmags
  (lambda (rt bufferA bufferB)
    (construct-ugen "FFTDiffMags" rt (list bufferA bufferB) nil 1 nil)))

(define fftflux
  (lambda (rt buffer normalise)
    (construct-ugen "FFTFlux" rt (list buffer normalise) nil 1 nil)))

(define fftfluxpos
  (lambda (rt buffer normalise)
    (construct-ugen "FFTFluxPos" rt (list buffer normalise) nil 1 nil)))

(define fftmkl
  (lambda (rt buffer epsilon)
    (construct-ugen "FFTMKL" rt (list buffer epsilon) nil 1 nil)))

(define fftpeak
  (lambda (rt buffer freqlo freqhi)
    (construct-ugen "FFTPeak" rt (list buffer freqlo freqhi) nil 2 nil)))

(define fftphasedev
  (lambda (rt buffer weight powthresh)
    (construct-ugen "FFTPhaseDev" rt (list buffer weight powthresh) nil 1 nil)))

(define fftpower
  (lambda (rt buffer square)
    (construct-ugen "FFTPower" rt (list buffer square) nil 1 nil)))

(define fftslope
  (lambda (rt buffer)
    (construct-ugen "FFTSlope" rt (list buffer) nil 1 nil)))

(define fftspread
  (lambda (rt buffer centroid)
    (construct-ugen "FFTSpread" rt (list buffer centroid) nil 1 nil)))

(define fftsubbandflatness
  (lambda (rt chain cutfreqs)
    (construct-ugen "FFTSubbandFlatness" rt (list chain cutfreqs) nil 1 nil)))

(define fftsubbandflux
  (lambda (rt chain cutfreqs posonly)
    (construct-ugen "FFTSubbandFlux" rt (list chain cutfreqs posonly) nil 1 nil)))

(define fftsubbandpower
  (lambda (rt chain cutfreqs square scalemode)
    (construct-ugen "FFTSubbandPower" rt (list chain cutfreqs square scalemode) nil 1 nil)))

(define fm7
  (lambda (rt ctlMatrix modMatrix)
    (construct-ugen "FM7" rt nil ctlMatrix modMatrix 6 nil)))

(define fmgrain
  (lambda (trigger dur carfreq modfreq index)
    (construct-ugen "FMGrain" (list 0) (list trigger dur carfreq modfreq index) nil 1 nil)))

(define fmgrainb
  (lambda (trigger dur carfreq modfreq index envbuf)
    (construct-ugen "FMGrainB" (list 0) (list trigger dur carfreq modfreq index envbuf) nil 1 nil)))

(define fmgrainbbf
  (lambda (rt trigger dur carfreq modfreq index envbuf azimuth elevation rho wComp)
    (construct-ugen "FMGrainBBF" rt (list trigger dur carfreq modfreq index envbuf azimuth elevation rho wComp) nil 4 nil)))

(define fmgrainbf
  (lambda (rt trigger dur carfreq modfreq index azimuth elevation rho wComp)
    (construct-ugen "FMGrainBF" rt (list trigger dur carfreq modfreq index azimuth elevation rho wComp) nil 4 nil)))

(define fmgraini
  (lambda (rt trigger dur carfreq modfreq index envbuf1 envbuf2 ifac)
    (construct-ugen "FMGrainI" rt (list trigger dur carfreq modfreq index envbuf1 envbuf2 ifac) nil 1 nil)))

(define fmgrainibf
  (lambda (rt trigger dur carfreq modfreq index envbuf1 envbuf2 ifac azimuth elevation rho wComp)
    (construct-ugen "FMGrainIBF" rt (list trigger dur carfreq modfreq index envbuf1 envbuf2 ifac azimuth elevation rho wComp) nil 4 nil)))

(define fmhdecode1
  (lambda (rt w x y z r s t u v azimuth elevation)
    (construct-ugen "FMHDecode1" rt (list w x y z r s t u v azimuth elevation) nil 1 nil)))

(define fmhencode0
  (lambda (rt input azimuth elevation gain)
    (construct-ugen "FMHEncode0" rt (list input azimuth elevation gain) nil 9 nil)))

(define fmhencode1
  (lambda (rt input azimuth elevation rho gain wComp)
    (construct-ugen "FMHEncode1" rt (list input azimuth elevation rho gain wComp) nil 9 nil)))

(define fmhencode2
  (lambda (rt input point_x point_y elevation gain wComp)
    (construct-ugen "FMHEncode2" rt (list input point_x point_y elevation gain wComp) nil 9 nil)))

(define featuresave
  (lambda (rt features trig_)
    (construct-ugen "FeatureSave" rt (list features trig_) nil 1 nil)))

(define fhn2dc
  (lambda (rt minfreq maxfreq urate wrate b0 b1 i u0 w0)
    (construct-ugen "Fhn2DC" rt (list minfreq maxfreq urate wrate b0 b1 i u0 w0) nil 1 nil)))

(define fhn2dl
  (lambda (rt minfreq maxfreq urate wrate b0 b1 i u0 w0)
    (construct-ugen "Fhn2DL" rt (list minfreq maxfreq urate wrate b0 b1 i u0 w0) nil 1 nil)))

(define fhn2dn
  (lambda (rt minfreq maxfreq urate wrate b0 b1 i u0 w0)
    (construct-ugen "Fhn2DN" rt (list minfreq maxfreq urate wrate b0 b1 i u0 w0) nil 1 nil)))

(define fhntrig
  (lambda (rt minfreq maxfreq urate wrate b0 b1 i u0 w0)
    (construct-ugen "FhnTrig" rt (list minfreq maxfreq urate wrate b0 b1 i u0 w0) nil 1 nil)))

(define fincosprottl
  (lambda (rt freq a h xi yi zi)
    (construct-ugen "FincoSprottL" rt (list freq a h xi yi zi) nil 3 nil)))

(define fincosprottm
  (lambda (rt freq a b h xi yi zi)
    (construct-ugen "FincoSprottM" rt (list freq a b h xi yi zi) nil 3 nil)))

(define fincosprotts
  (lambda (rt freq a b h xi yi zi)
    (construct-ugen "FincoSprottS" rt (list freq a b h xi yi zi) nil 3 nil)))

(define fitzhughnagumo
  (lambda (rt reset rateu ratew b0 b1 initu initw)
    (construct-ugen "FitzHughNagumo" rt (list reset rateu ratew b0 b1 initu initw) nil 1 nil)))

(define framecompare
  (lambda (rt buffer1 buffer2 wAmount)
    (construct-ugen "FrameCompare" rt (list buffer1 buffer2 wAmount) nil 1 nil)))

(define friction
  (lambda (rt input friction spring damp mass beltmass)
    (construct-ugen "Friction" rt (list input friction spring damp mass beltmass) nil 1 nil)))

(define gammatone
  (lambda (input centrefrequency bandwidth)
    (construct-ugen "Gammatone" (list 0) (list input centrefrequency bandwidth) nil 1 nil)))

(define gaussclass
  (lambda (rt input bufnum gate_)
    (construct-ugen "GaussClass" rt (list input bufnum gate_) nil 1 nil)))

(define gausstrig
  (lambda (rt freq dev)
    (construct-ugen "GaussTrig" rt (list freq dev) nil 1 nil)))

(define gbman2dc
  (lambda (rt minfreq maxfreq x0 y0)
    (construct-ugen "Gbman2DC" rt (list minfreq maxfreq x0 y0) nil 1 nil)))

(define gbman2dl
  (lambda (rt minfreq maxfreq x0 y0)
    (construct-ugen "Gbman2DL" rt (list minfreq maxfreq x0 y0) nil 1 nil)))

(define gbman2dn
  (lambda (rt minfreq maxfreq x0 y0)
    (construct-ugen "Gbman2DN" rt (list minfreq maxfreq x0 y0) nil 1 nil)))

(define gbmantrig
  (lambda (rt minfreq maxfreq x0 y0)
    (construct-ugen "GbmanTrig" rt (list minfreq maxfreq x0 y0) nil 1 nil)))

(define gendy4
  (lambda (rt ampdist durdist adparam ddparam minfreq maxfreq ampscale durscale initCPs knum)
    (construct-ugen "Gendy4" rt (list ampdist durdist adparam ddparam minfreq maxfreq ampscale durscale initCPs knum) nil 1 nil)))

(define gendy5
  (lambda (rt ampdist durdist adparam ddparam minfreq maxfreq ampscale durscale initCPs knum)
    (construct-ugen "Gendy5" rt (list ampdist durdist adparam ddparam minfreq maxfreq ampscale durscale initCPs knum) nil 1 nil)))

(define u:getenv
  (lambda (rt key defaultval)
    (construct-ugen "Getenv" rt (list key defaultval) nil 1 nil)))

(define glitchbpf
  (lambda (rt input freq rq)
    (construct-ugen "GlitchBPF" rt (list input freq rq) nil 1 nil)))

(define glitchbrf
  (lambda (rt input freq rq)
    (construct-ugen "GlitchBRF" rt (list input freq rq) nil 1 nil)))

(define glitchhpf
  (lambda (rt input freq)
    (construct-ugen "GlitchHPF" rt (list input freq) nil 1 nil)))

(define glitchrhpf
  (lambda (input freq rq)
    (construct-ugen "GlitchRHPF" (list 0) (list input freq rq) nil 1 nil)))

(define goertzel
  (lambda (rt input bufsize freq hop)
    (construct-ugen "Goertzel" rt (list input bufsize freq hop) nil 2 nil)))

(define grainbufj
  (lambda (rt numChannels trigger dur sndbuf rate_ pos loop interp grainAmp pan envbufnum maxGrains)
    (construct-ugen "GrainBufJ" rt (list numChannels trigger dur sndbuf rate_ pos loop interp grainAmp pan envbufnum maxGrains) nil 1 nil)))

(define grainfmj
  (lambda (rt numChannels trigger dur carfreq modfreq index grainAmp pan envbufnum maxGrains)
    (construct-ugen "GrainFMJ" rt (list numChannels trigger dur carfreq modfreq index grainAmp pan envbufnum maxGrains) nil 1 nil)))

(define graininj
  (lambda (rt numChannels trigger dur input grainAmp pan envbufnum maxGrains)
    (construct-ugen "GrainInJ" rt (list numChannels trigger dur input grainAmp pan envbufnum maxGrains) nil 1 nil)))

(define grainsinj
  (lambda (rt numChannels trigger dur freq grainAmp pan envbufnum maxGrains)
    (construct-ugen "GrainSinJ" rt (list numChannels trigger dur freq grainAmp pan envbufnum maxGrains) nil 1 nil)))

(define gravitygrid
  (lambda (rt reset rate_ newx newy bufnum)
    (construct-ugen "GravityGrid" rt (list reset rate_ newx newy bufnum) nil 1 nil)))

(define gravitygrid2
  (lambda (rt reset rate_ newx newy bufnum)
    (construct-ugen "GravityGrid2" rt (list reset rate_ newx newy bufnum) nil 1 nil)))

(define greyholeraw
  (lambda (in1 in2 damping delaytime diffusion feedback moddepth modfreq size)
    (construct-ugen "GreyholeRaw" (list 0 1) (list in1 in2 damping delaytime diffusion feedback moddepth modfreq size) nil 2 nil)))

(define haircell
  (lambda (input spontaneousrate boostrate restorerate loss)
    (construct-ugen "HairCell" (list 0) (list input spontaneousrate boostrate restorerate loss) nil 1 nil)))

(define harmonicosc
  (lambda (rt freq firstharmonic amplitudes)
    (construct-ugen "HarmonicOsc" rt (list freq firstharmonic) amplitudes 1 nil)))

(define henon2dc
  (lambda (rt minfreq maxfreq a b x0 y0)
    (construct-ugen "Henon2DC" rt (list minfreq maxfreq a b x0 y0) nil 1 nil)))

(define henon2dl
  (lambda (rt minfreq maxfreq a b x0 y0)
    (construct-ugen "Henon2DL" rt (list minfreq maxfreq a b x0 y0) nil 1 nil)))

(define henon2dn
  (lambda (rt minfreq maxfreq a b x0 y0)
    (construct-ugen "Henon2DN" rt (list minfreq maxfreq a b x0 y0) nil 1 nil)))

(define henontrig
  (lambda (rt minfreq maxfreq a b x0 y0)
    (construct-ugen "HenonTrig" rt (list minfreq maxfreq a b x0 y0) nil 1 nil)))

(define icepstrum
  (lambda (rt cepchain fftbuf)
    (construct-ugen "ICepstrum" rt (list cepchain fftbuf) nil 1 nil)))

(define iirfilter
  (lambda (rt input freq rq)
    (construct-ugen "IIRFilter" rt (list input freq rq) nil 1 nil)))

(define ingrain
  (lambda (rt trigger dur input)
    (construct-ugen "InGrain" rt (list trigger dur input) nil 1 nil)))

(define ingrainb
  (lambda (rt trigger dur input envbuf)
    (construct-ugen "InGrainB" rt (list trigger dur input envbuf) nil 1 nil)))

(define ingrainbbf
  (lambda (rt trigger dur input envbuf azimuth elevation rho wComp)
    (construct-ugen "InGrainBBF" rt (list trigger dur input envbuf azimuth elevation rho wComp) nil 4 nil)))

(define ingrainbf
  (lambda (rt trigger dur input azimuth elevation rho wComp)
    (construct-ugen "InGrainBF" rt (list trigger dur input azimuth elevation rho wComp) nil 4 nil)))

(define ingraini
  (lambda (rt trigger dur input envbuf1 envbuf2 ifac)
    (construct-ugen "InGrainI" rt (list trigger dur input envbuf1 envbuf2 ifac) nil 1 nil)))

(define ingrainibf
  (lambda (rt trigger dur input envbuf1 envbuf2 ifac azimuth elevation rho wComp)
    (construct-ugen "InGrainIBF" rt (list trigger dur input envbuf1 envbuf2 ifac azimuth elevation rho wComp) nil 4 nil)))

(define insideout
  (lambda (rt input)
    (construct-ugen "InsideOut" rt (list input) nil 1 nil)))

(define instruction
  (lambda (rt bufnum)
    (construct-ugen "Instruction" rt (list bufnum) nil 1 nil)))

(define jpverbraw
  (lambda (in1 in2 damp earlydiff highband highx lowband lowx mdepth mfreq midx size t60)
    (construct-ugen "JPverbRaw" (list 0) (list in1 in2 damp earlydiff highband highx lowband lowx mdepth mfreq midx size t60) nil 2 nil)))

(define kmeansrt
  (lambda (rt bufnum inputdata k gate_ reset learn)
    (construct-ugen "KMeansRT" rt (list bufnum inputdata k gate_ reset learn) nil 1 nil)))

(define keyclarity
  (lambda (rt chain keydecay chromaleak)
    (construct-ugen "KeyClarity" rt (list chain keydecay chromaleak) nil 1 nil)))

(define keymode
  (lambda (rt chain keydecay chromaleak)
    (construct-ugen "KeyMode" rt (list chain keydecay chromaleak) nil 1 nil)))

(define kmeanstobpset1
  (lambda (rt freq numdatapoints maxnummeans nummeans tnewdata tnewmeans soft bufnum)
    (construct-ugen "KmeansToBPSet1" rt (list freq numdatapoints maxnummeans nummeans tnewdata tnewmeans soft bufnum) nil 1 nil)))

(define ladspa
  (lambda (rt nChans id_ args)
    (construct-ugen "LADSPA" rt (list nChans id_ args) nil 0 nil)))

(define lfbrownnoise0
  (lambda (rt freq dev dist)
    (construct-ugen "LFBrownNoise0" rt (list freq dev dist) nil 1 nil)))

(define lfbrownnoise1
  (lambda (rt freq dev dist)
    (construct-ugen "LFBrownNoise1" rt (list freq dev dist) nil 1 nil)))

(define lfbrownnoise2
  (lambda (rt freq dev dist)
    (construct-ugen "LFBrownNoise2" rt (list freq dev dist) nil 1 nil)))

(define lpcanalyzer
  (lambda (input source n p testE delta windowtype)
    (construct-ugen "LPCAnalyzer" (list 0 1) (list input source n p testE delta windowtype) nil 1 nil)))

(define lpcerror
  (lambda (rt input p)
    (construct-ugen "LPCError" rt (list input p) nil 1 nil)))

(define lpcsynth
  (lambda (buffer signal pointer)
    (construct-ugen "LPCSynth" audio-rate (list buffer signal pointer) nil 1 nil)))

(define lpcvals
  (lambda (buffer pointer)
    (construct-ugen "LPCVals" audio-rate (list buffer pointer) nil 3 nil)))

(define lpf1
  (lambda (rt input freq)
    (construct-ugen "LPF1" rt (list input freq) nil 1 nil)))

(define lpf18
  (lambda (rt input freq res dist)
    (construct-ugen "LPF18" rt (list input freq res dist) nil 1 nil)))

(define lpfvs6
  (lambda (rt input freq slope)
    (construct-ugen "LPFVS6" rt (list input freq slope) nil 1 nil)))

(define lpg
  (lambda (input controlinput controloffset controlscale vca resonance lowpassmode linearity)
    (construct-ugen "LPG" (list 0) (list input controlinput controloffset controlscale vca resonance lowpassmode linearity) nil 1 nil)))

(define lti
  (lambda (rt input bufnuma bufnumb)
    (construct-ugen "LTI" rt (list input bufnuma bufnumb) nil 1 nil)))

(define latoocarfian2dc
  (lambda (rt minfreq maxfreq a b c d x0 y0)
    (construct-ugen "Latoocarfian2DC" rt (list minfreq maxfreq a b c d x0 y0) nil 1 nil)))

(define latoocarfian2dl
  (lambda (rt minfreq maxfreq a b c d x0 y0)
    (construct-ugen "Latoocarfian2DL" rt (list minfreq maxfreq a b c d x0 y0) nil 1 nil)))

(define latoocarfian2dn
  (lambda (rt minfreq maxfreq a b c d x0 y0)
    (construct-ugen "Latoocarfian2DN" rt (list minfreq maxfreq a b c d x0 y0) nil 1 nil)))

(define latoocarfiantrig
  (lambda (rt minfreq maxfreq a b c d x0 y0)
    (construct-ugen "LatoocarfianTrig" rt (list minfreq maxfreq a b c d x0 y0) nil 1 nil)))

(define listtrig
  (lambda (rt bufnum reset offset numframes)
    (construct-ugen "ListTrig" rt (list bufnum reset offset numframes) nil 1 nil)))

(define listtrig2
  (lambda (rt bufnum reset numframes)
    (construct-ugen "ListTrig2" rt (list bufnum reset numframes) nil 1 nil)))

(define logger
  (lambda (rt inputArray trig_ bufnum reset)
    (construct-ugen "Logger" rt (list inputArray trig_ bufnum reset) nil 1 nil)))

(define loopbuf
  (lambda (nc rt bufnum rate_ gate_ startPos startLoop endLoop interpolation)
    (construct-ugen "LoopBuf" rt (list bufnum rate_ gate_ startPos startLoop endLoop interpolation) nil nc nil)))

(define lorenz2dc
  (lambda (rt minfreq maxfreq s r b h x0 y0 z0)
    (construct-ugen "Lorenz2DC" rt (list minfreq maxfreq s r b h x0 y0 z0) nil 1 nil)))

(define lorenz2dl
  (lambda (rt minfreq maxfreq s r b h x0 y0 z0)
    (construct-ugen "Lorenz2DL" rt (list minfreq maxfreq s r b h x0 y0 z0) nil 1 nil)))

(define lorenz2dn
  (lambda (rt minfreq maxfreq s r b h x0 y0 z0)
    (construct-ugen "Lorenz2DN" rt (list minfreq maxfreq s r b h x0 y0 z0) nil 1 nil)))

(define lorenztrig
  (lambda (rt minfreq maxfreq s r b h x0 y0 z0)
    (construct-ugen "LorenzTrig" rt (list minfreq maxfreq s r b h x0 y0 z0) nil 1 nil)))

(define lores
  (lambda (input freq res)
    (construct-ugen "Lores" (list 0) (list input freq res) nil 1 nil)))

(define lotkavolterra
  (lambda (rt freq a b c d h xi yi)
    (construct-ugen "LotkaVolterra" rt (list freq a b c d h xi yi) nil 2 nil)))

(define mcldchaosgen
  (lambda (rt maxSize)
    (construct-ugen "MCLDChaosGen" rt (list maxSize) nil 1 nil)))

(define mzpokey
  (lambda (rt audf1 audc1 audf2 audc2 audf3 audc3 audf4 audc4 audctl)
    (construct-ugen "MZPokey" rt (list audf1 audc1 audf2 audc2 audf3 audc3 audf4 audc4 audctl) nil 1 nil)))

(define markovsynth
  (lambda (rt input isRecording waitTime tableSize)
    (construct-ugen "MarkovSynth" rt (list input isRecording waitTime tableSize) nil 1 nil)))

(define matchingp
  (lambda (rt dict input dictsize ntofind hop method)
    (construct-ugen "MatchingP" rt (list dict input dictsize ntofind hop method) nil 4 nil)))

(define matchingpresynth
  (lambda (rt dict method trigger residual activs)
    (construct-ugen "MatchingPResynth" rt (list dict method trigger residual activs) nil 1 nil)))

(define maxamp
  (lambda (rt input numSamps)
    (construct-ugen "Maxamp" rt (list input numSamps) nil 1 nil)))

(define mdapiano
  (lambda (rt freq gate_ vel decay_ release hard velhard muffle velmuff velcurve stereo tune random stretch sustain)
    (construct-ugen "MdaPiano" rt (list freq gate_ vel decay_ release hard velhard muffle velmuff velcurve stereo tune random stretch sustain) nil 2 nil)))

(define meantriggered
  (lambda (rt input trig_ length_)
    (construct-ugen "MeanTriggered" rt (list input trig_ length_) nil 1 nil)))

(define meddis
  (lambda (input)
    (construct-ugen "Meddis" (list 0) (list input) nil 1 nil)))

(define medianseparation
  (lambda (rt fft fftharmonic fftpercussive fftsize mediansize hardorsoft p medianormax)
    (construct-ugen "MedianSeparation" rt (list fft fftharmonic fftpercussive fftsize mediansize hardorsoft p medianormax) nil 2 nil)))

(define mediantriggered
  (lambda (rt input trig_ length_)
    (construct-ugen "MedianTriggered" rt (list input trig_ length_) nil 1 nil)))

(define membranecircle
  (lambda (rt excitation tension loss)
    (construct-ugen "MembraneCircle" rt (list excitation tension loss) nil 1 nil)))

(define membranehexagon
  (lambda (rt excitation tension loss)
    (construct-ugen "MembraneHexagon" rt (list excitation tension loss) nil 1 nil)))

(define metro
  (lambda (rt bpm numBeats)
    (construct-ugen "Metro" rt (list bpm numBeats) nil 1 nil)))

(define mibraids
  (lambda (rt pitch timbre color model trig_ resamp decim bits ws)
    (construct-ugen "MiBraids" rt (list pitch timbre color model trig_ resamp decim bits ws) nil 1 nil)))

(define miclouds
  (lambda (rt pit pos size dens tex drywet in_gain spread rvb fb freeze mode lofi trig_ inputArray)
    (construct-ugen "MiClouds" rt (list pit pos size dens tex drywet in_gain spread rvb fb freeze mode lofi trig_) inputArray 2 nil)))

(define mielements
  (lambda (rt blow_in strike_in gate_ pit strength contour bow_level blow_level strike_level flow mallet bow_timb blow_timb strike_timb geom bright damp pos space model easteregg)
    (construct-ugen "MiElements" rt (list blow_in strike_in gate_ pit strength contour bow_level blow_level strike_level flow mallet bow_timb blow_timb strike_timb geom bright damp pos space model easteregg) nil 2 nil)))

(define migrids
  (lambda (rt bpm map_x map_y chaos bd_density sd_density hh_density mode swing config)
    (construct-ugen "MiGrids" rt (list bpm map_x map_y chaos bd_density sd_density hh_density mode swing config) nil 6 nil)))

(define mimu
  (lambda (rt input gain bypass)
    (construct-ugen "MiMu" rt (list input gain bypass) nil 1 nil)))

(define miomi
  (lambda (rt audio_in gate_ pit contour detune level1 level2 ratio1 ratio2 fm1 fm2 fb xfb filter_mode cutoff reson strength env rotate space)
    (construct-ugen "MiOmi" rt (list audio_in gate_ pit contour detune level1 level2 ratio1 ratio2 fm1 fm2 fb xfb filter_mode cutoff reson strength env rotate space) nil 2 nil)))

(define miplaits
  (lambda (rt pitch engine harm timbre morph trigger level fm_mod timb_mod morph_mod decay_ lpg_colour)
    (construct-ugen "MiPlaits" rt (list pitch engine harm timbre morph trigger level fm_mod timb_mod morph_mod decay_ lpg_colour) nil 2 nil)))

(define mirings
  (lambda (rt input trig_ pit struct bright damp pos model poly intern_exciter easteregg bypass)
    (construct-ugen "MiRings" rt (list input trig_ pit struct bright damp pos model poly intern_exciter easteregg bypass) nil 2 nil)))

(define miripples
  (lambda (input cf reson drive)
    (construct-ugen "MiRipples" (list 0) (list input cf reson drive) nil 1 nil)))

(define mitides
  (lambda (rt freq shape slope smooth shift trig_ clock output_mode ramp_mode ratio rate_)
    (construct-ugen "MiTides" rt (list freq shape slope smooth shift trig_ clock output_mode ramp_mode ratio rate_) nil 4 nil)))

(define miverb
  (lambda (time drywet damp hp freeze diff inputArray)
    (construct-ugen "MiVerb" (list 6) (list time drywet damp hp freeze diff) inputArray 2 nil)))

(define miwarps
  (lambda (rt carrier modulator lev1 lev2 algo timb osc freq vgain easteregg)
    (construct-ugen "MiWarps" rt (list carrier modulator lev1 lev2 algo timb osc freq vgain easteregg) nil 2 nil)))

(define monograin
  (lambda (rt input winsize grainrate winrandpct)
    (construct-ugen "MonoGrain" rt (list input winsize grainrate winrandpct) nil 1 nil)))

(define monograinbf
  (lambda (rt input winsize grainrate winrandpct azimuth azrand elevation elrand rho)
    (construct-ugen "MonoGrainBF" rt (list input winsize grainrate winrandpct azimuth azrand elevation elrand rho) nil 4 nil)))

(define moogladder
  (lambda (input ffreq res)
    (construct-ugen "MoogLadder" (list 0) (list input ffreq res) nil 1 nil)))

(define moogvcf
  (lambda (input fco res)
    (construct-ugen "MoogVCF" (list 0) (list input fco res) nil 1 nil)))

(define nl
  (lambda (rt input bufnuma bufnumb guard1 guard2)
    (construct-ugen "NL" rt (list input bufnuma bufnumb guard1 guard2) nil 1 nil)))

(define nl2
  (lambda (rt input bufnum maxsizea maxsizeb guard1 guard2)
    (construct-ugen "NL2" rt (list input bufnum maxsizea maxsizeb guard1 guard2) nil 1 nil)))

(define nlfiltc
  (lambda (rt input a b d c l)
    (construct-ugen "NLFiltC" rt (list input a b d c l) nil 1 nil)))

(define nlfiltl
  (lambda (rt input a b d c l)
    (construct-ugen "NLFiltL" rt (list input a b d c l) nil 1 nil)))

(define nlfiltn
  (lambda (rt input a b d c l)
    (construct-ugen "NLFiltN" rt (list input a b d c l) nil 1 nil)))

(define ntube
  (lambda (rt input lossarray karray delaylengtharray)
    (construct-ugen "NTube" rt (list input lossarray karray delaylengtharray) nil 1 nil)))

(define nearestn
  (lambda (rt treebuf input gate_ num)
    (construct-ugen "NearestN" rt (list treebuf input gate_ num) nil 3 nil)))

(define needlerect
  (lambda (rt rate_ imgWidth imgHeight rectX rectY rectW rectH)
    (construct-ugen "NeedleRect" rt (list rate_ imgWidth imgHeight rectX rectY rectW rectH) nil 1 nil)))

(define neoformant
  (lambda (rt formantfreq carrierfreq phaseshift)
    (construct-ugen "NeoFormant" rt (list formantfreq carrierfreq phaseshift) nil 1 nil)))

(define neovarsawosc
  (lambda (rt freq pw waveshape)
    (construct-ugen "NeoVarSawOsc" rt (list freq pw waveshape) nil 1 nil)))

(define nes2
  (lambda (rt trig_ a0 a1 a2 a3 b0 b1 b2 b3 c0 c2 c3 d0 d2 d3 e0 e1 e2 e3 smask)
    (construct-ugen "Nes2" rt (list trig_ a0 a1 a2 a3 b0 b1 b2 b3 c0 c2 c3 d0 d2 d3 e0 e1 e2 e3 smask) nil 1 nil)))

(define nestedallpassc
  (lambda (input maxdelay1 delay1 gain1 maxdelay2 delay2 gain2)
    (construct-ugen "NestedAllpassC" (list 0) (list input maxdelay1 delay1 gain1 maxdelay2 delay2 gain2) nil 1 nil)))

(define nestedallpassl
  (lambda (input maxdelay1 delay1 gain1 maxdelay2 delay2 gain2)
    (construct-ugen "NestedAllpassL" (list 0) (list input maxdelay1 delay1 gain1 maxdelay2 delay2 gain2) nil 1 nil)))

(define nestedallpassn
  (lambda (input maxdelay1 delay1 gain1 maxdelay2 delay2 gain2)
    (construct-ugen "NestedAllpassN" (list 0) (list input maxdelay1 delay1 gain1 maxdelay2 delay2 gain2) nil 1 nil)))

(define osfold4
  (lambda (rt input lo hi)
    (construct-ugen "OSFold4" rt (list input lo hi) nil 1 nil)))

(define osfold8
  (lambda (rt input lo hi)
    (construct-ugen "OSFold8" rt (list input lo hi) nil 1 nil)))

(define ostrunc4
  (lambda (rt input quant)
    (construct-ugen "OSTrunc4" rt (list input quant) nil 1 nil)))

(define ostrunc8
  (lambda (rt input quant)
    (construct-ugen "OSTrunc8" rt (list input quant) nil 1 nil)))

(define oswrap4
  (lambda (rt input lo hi)
    (construct-ugen "OSWrap4" rt (list input lo hi) nil 1 nil)))

(define oswrap8
  (lambda (rt input lo hi)
    (construct-ugen "OSWrap8" rt (list input lo hi) nil 1 nil)))

(define onsetstatistics
  (lambda (rt input windowsize hopsize)
    (construct-ugen "OnsetStatistics" rt (list input windowsize hopsize) nil 3 nil)))

(define oregonator
  (lambda (rt reset rate_ epsilon mu q initx inity initz)
    (construct-ugen "Oregonator" rt (list reset rate_ epsilon mu q initx inity initz) nil 3 nil)))

(define oscbank
  (lambda (rt freq gain saw8 square8 saw4 square4 saw2 square2 saw1)
    (construct-ugen "OscBank" rt (list freq gain saw8 square8 saw4 square4 saw2 square2 saw1) nil 1 nil)))

(define oteypiano
  (lambda (rt freq vel t_gate rmin rmax rampl rampr rcore lmin lmax lampl lampr rho e zb zh mh k alpha p hpos loss detune hammer_type)
    (construct-ugen "OteyPiano" rt (list freq vel t_gate rmin rmax rampl rampr rcore lmin lmax lampl lampr rho e zb zh mh k alpha p hpos loss detune hammer_type) nil 1 nil)))

(define oteypianostrings
  (lambda (rt freq vel t_gate rmin rmax rampl rampr rcore lmin lmax lampl lampr rho e zb zh mh k alpha p hpos loss detune hammer_type)
    (construct-ugen "OteyPianoStrings" rt (list freq vel t_gate rmin rmax rampl rampr rcore lmin lmax lampl lampr rho e zb zh mh k alpha p hpos loss detune hammer_type) nil 1 nil)))

(define oteysoundboard
  (lambda (inp c1 c3 mix)
    (construct-ugen "OteySoundBoard" (list 0) (list inp c1 c3 mix) nil 1 nil)))

(define pvinfo
  (lambda (rt pvbuffer binNum filePointer)
    (construct-ugen "PVInfo" rt (list pvbuffer binNum filePointer) nil 2 nil)))

(define pvsynth
  (lambda (rt pvbuffer numBins binStart binSkip filePointer freqMul freqAdd)
    (construct-ugen "PVSynth" rt (list pvbuffer numBins binStart binSkip filePointer freqMul freqAdd) nil 1 nil)))

(define pv_binbufrd
  (lambda (buffer playbuf point binStart binSkip numBins clear)
    (construct-ugen "PV_BinBufRd" control-rate (list buffer playbuf point binStart binSkip numBins clear) nil 1 nil)))

(define pv_bindelay
  (lambda (buffer maxdelay delaybuf fbbuf hop)
    (construct-ugen "PV_BinDelay" control-rate (list buffer maxdelay delaybuf fbbuf hop) nil 1 nil)))

(define pv_binfilter
  (lambda (buffer start end)
    (construct-ugen "PV_BinFilter" control-rate (list buffer start end) nil 1 nil)))

(define pv_binplaybuf
  (lambda (buffer playbuf rate_ offset binStart binSkip numBins loop clear)
    (construct-ugen "PV_BinPlayBuf" control-rate (list buffer playbuf rate_ offset binStart binSkip numBins loop clear) nil 1 nil)))

(define pv_bufrd
  (lambda (buffer playbuf point)
    (construct-ugen "PV_BufRd" control-rate (list buffer playbuf point) nil 1 nil)))

(define pv_commonmag
  (lambda (bufferA bufferB tolerance remove)
    (construct-ugen "PV_CommonMag" control-rate (list bufferA bufferB tolerance remove) nil 1 nil)))

(define pv_commonmul
  (lambda (bufferA bufferB tolerance remove)
    (construct-ugen "PV_CommonMul" control-rate (list bufferA bufferB tolerance remove) nil 1 nil)))

(define pv_compander
  (lambda (buffer thresh slopeBelow slopeAbove)
    (construct-ugen "PV_Compander" control-rate (list buffer thresh slopeBelow slopeAbove) nil 1 nil)))

(define pv_cutoff
  (lambda (bufferA bufferB wipe)
    (construct-ugen "PV_Cutoff" control-rate (list bufferA bufferB wipe) nil 1 nil)))

(define pv_evenbin
  (lambda (buffer)
    (construct-ugen "PV_EvenBin" control-rate (list buffer) nil 1 nil)))

(define pv_extractrepeat
  (lambda (buffer loopbuf loopdur memorytime which ffthop thresh)
    (construct-ugen "PV_ExtractRepeat" control-rate (list buffer loopbuf loopdur memorytime which ffthop thresh) nil 1 nil)))

(define pv_freeze
  (lambda (buffer freeze)
    (construct-ugen "PV_Freeze" control-rate (list buffer freeze) nil 1 nil)))

(define pv_freqbuffer
  (lambda (buffer databuffer)
    (construct-ugen "PV_FreqBuffer" control-rate (list buffer databuffer) nil 1 nil)))

(define pv_invert
  (lambda (buffer)
    (construct-ugen "PV_Invert" control-rate (list buffer) nil 1 nil)))

(define pv_magbuffer
  (lambda (buffer databuffer)
    (construct-ugen "PV_MagBuffer" control-rate (list buffer databuffer) nil 1 nil)))

(define pv_magexp
  (lambda (buffer)
    (construct-ugen "PV_MagExp" control-rate (list buffer) nil 1 nil)))

(define pv_maggate
  (lambda (buffer thresh remove)
    (construct-ugen "PV_MagGate" control-rate (list buffer thresh remove) nil 1 nil)))

(define pv_maglog
  (lambda (buffer)
    (construct-ugen "PV_MagLog" control-rate (list buffer) nil 1 nil)))

(define pv_magmap
  (lambda (buffer mapbuf)
    (construct-ugen "PV_MagMap" control-rate (list buffer mapbuf) nil 1 nil)))

(define pv_magminus
  (lambda (bufferA bufferB remove)
    (construct-ugen "PV_MagMinus" control-rate (list bufferA bufferB remove) nil 1 nil)))

(define pv_magmuladd
  (lambda (buffer)
    (construct-ugen "PV_MagMulAdd" control-rate (list buffer) nil 1 nil)))

(define pv_magscale
  (lambda (bufferA bufferB)
    (construct-ugen "PV_MagScale" control-rate (list bufferA bufferB) nil 1 nil)))

(define pv_magsmooth
  (lambda (buffer factor)
    (construct-ugen "PV_MagSmooth" control-rate (list buffer factor) nil 1 nil)))

(define pv_magsubtract
  (lambda (bufferA bufferB zerolimit)
    (construct-ugen "PV_MagSubtract" control-rate (list bufferA bufferB zerolimit) nil 1 nil)))

(define pv_maxmagn
  (lambda (buffer numbins)
    (construct-ugen "PV_MaxMagN" control-rate (list buffer numbins) nil 1 nil)))

(define pv_minmagn
  (lambda (buffer numbins)
    (construct-ugen "PV_MinMagN" control-rate (list buffer numbins) nil 1 nil)))

(define pv_morph
  (lambda (bufferA bufferB morph)
    (construct-ugen "PV_Morph" control-rate (list bufferA bufferB morph) nil 1 nil)))

(define pv_noisesynthf
  (lambda (buffer threshold numFrames initflag)
    (construct-ugen "PV_NoiseSynthF" control-rate (list buffer threshold numFrames initflag) nil 1 nil)))

(define pv_noisesynthp
  (lambda (buffer threshold numFrames initflag)
    (construct-ugen "PV_NoiseSynthP" control-rate (list buffer threshold numFrames initflag) nil 1 nil)))

(define pv_oddbin
  (lambda (buffer)
    (construct-ugen "PV_OddBin" control-rate (list buffer) nil 1 nil)))

(define pv_partialsynthf
  (lambda (buffer threshold numFrames initflag)
    (construct-ugen "PV_PartialSynthF" control-rate (list buffer threshold numFrames initflag) nil 1 nil)))

(define pv_partialsynthp
  (lambda (buffer threshold numFrames initflag)
    (construct-ugen "PV_PartialSynthP" control-rate (list buffer threshold numFrames initflag) nil 1 nil)))

(define pv_pitchshift
  (lambda (buffer ratio)
    (construct-ugen "PV_PitchShift" control-rate (list buffer ratio) nil 1 nil)))

(define pv_playbuf
  (lambda (buffer playbuf rate_ offset loop)
    (construct-ugen "PV_PlayBuf" control-rate (list buffer playbuf rate_ offset loop) nil 1 nil)))

(define pv_recordbuf
  (lambda (buffer recbuf offset run loop hop wintype)
    (construct-ugen "PV_RecordBuf" control-rate (list buffer recbuf offset run loop hop wintype) nil 1 nil)))

(define pv_softwipe
  (lambda (bufferA bufferB wipe)
    (construct-ugen "PV_SoftWipe" control-rate (list bufferA bufferB wipe) nil 1 nil)))

(define pv_spectralenhance
  (lambda (buffer numPartials ratio strength)
    (construct-ugen "PV_SpectralEnhance" control-rate (list buffer numPartials ratio strength) nil 1 nil)))

(define pv_spectralmap
  (lambda (buffer specBuffer floor_ freeze mode norm window)
    (construct-ugen "PV_SpectralMap" control-rate (list buffer specBuffer floor_ freeze mode norm window) nil 1 nil)))

(define pv_split
  (lambda (bufferA bufferB)
    (construct-ugen "PV_Split" control-rate (list bufferA bufferB) nil 2 nil)))

(define pv_whiten
  (lambda (chain trackbufnum relaxtime floor_ smear bindownsample)
    (construct-ugen "PV_Whiten" control-rate (list chain trackbufnum relaxtime floor_ smear bindownsample) nil 1 nil)))

(define pv_xfade
  (lambda (bufferA bufferB fade)
    (construct-ugen "PV_XFade" control-rate (list bufferA bufferB fade) nil 1 nil)))

(define panx
  (lambda (rt numChans input pos level width)
    (construct-ugen "PanX" rt (list numChans input pos level width) nil 0 nil)))

(define panx2d
  (lambda (rt numChansX numChansY input posX posY level widthX widthY)
    (construct-ugen "PanX2D" rt (list numChansX numChansY input posX posY level widthX widthY) nil 0 nil)))

(define peakeq2
  (lambda (rt input freq rs db)
    (construct-ugen "PeakEQ2" rt (list input freq rs db) nil 1 nil)))

(define peakeq4
  (lambda (rt input freq rs db)
    (construct-ugen "PeakEQ4" rt (list input freq rs db) nil 1 nil)))

(define perlin3
  (lambda (rt x y z)
    (construct-ugen "Perlin3" rt (list x y z) nil 1 nil)))

(define permmod
  (lambda (rt input freq)
    (construct-ugen "PermMod" rt (list input freq) nil 1 nil)))

(define permmodarray
  (lambda (rt input freq pattern_)
    (construct-ugen "PermModArray" rt (list input freq pattern_) nil 1 nil)))

(define permmodt
  (lambda (rt input outfreq infreq)
    (construct-ugen "PermModT" rt (list input outfreq infreq) nil 1 nil)))

(define phasormodal
  (lambda (input freq decay_ damp amp phase)
    (construct-ugen "PhasorModal" (list 0) (list input freq decay_ damp amp phase) nil 1 nil)))

(define planetree
  (lambda (rt treebuf input gate_)
    (construct-ugen "PlaneTree" rt (list treebuf input gate_) nil 1 nil)))

(define pokey
  (lambda (rt audf1 audc1 audf2 audc2 audf3 audc3 audf4 audc4 audctl)
    (construct-ugen "Pokey" rt (list audf1 audc1 audf2 audc2 audf3 audc3 audf4 audc4 audctl) nil 1 nil)))

(define posratio
  (lambda (rt input period thresh)
    (construct-ugen "PosRatio" rt (list input period thresh) nil 1 nil)))

(define printval
  (lambda (rt input numblocks id_)
    (construct-ugen "PrintVal" rt (list input numblocks id_) nil 1 nil)))

(define qitch
  (lambda (rt input databufnum ampThreshold algoflag ampbufnum minfreq maxfreq)
    (construct-ugen "Qitch" rt (list input databufnum ampThreshold algoflag ampbufnum minfreq maxfreq) nil 2 nil)))

(define bezier
  (lambda (rt haltAfter dx freq phase param)
    (construct-ugen "Bezier" rt (list haltAfter dx freq phase) param 1 nil)))

(define rcd
  (lambda (clock rotate reset div_ spread auto len down gates)
    (construct-ugen "RCD" (list 0) (list clock rotate reset div_ spread auto len down gates) nil 8 nil)))

(define scm
  (lambda (rt clock bpm rotate slip shuffle skip pw)
    (construct-ugen "SCM" rt (list clock bpm rotate slip shuffle skip pw) nil 8 nil)))

(define rdl
  (lambda (rt numChannels inputArray)
    (construct-ugen "RDL" rt (list numChannels) inputArray 1 nil)))

(define dx7
  (lambda (rt bufnum on off data_ vc mnn vel pw mw bc fc)
    (construct-ugen "DX7" rt (list bufnum on off data_ vc mnn vel pw mw bc fc) nil 1 nil)))

(define rdx7env
  (lambda (rt gate_ data_ r1 r2 r3 r4 l1 l2 l3 l4 ol)
    (construct-ugen "RDX7Env" rt (list gate_ data_ r1 r2 r3 r4 l1 l2 l3 l4 ol) nil 1 nil)))

(define rdelaymap
  (lambda (bufnum input dynamic spec)
    (construct-ugen "RDelayMap" (list 1) (list bufnum input dynamic) spec 1 nil)))

(define rdelayset
  (lambda (input spec)
    (construct-ugen "RDelaySet" (list 0) (list input) spec 1 nil)))

(define rdelaysetbuf
  (lambda (rt bufnum input spec)
    (construct-ugen "RDelaySetBuf" rt (list bufnum input) spec 1 nil)))

(define dustrange
  (lambda (iotMin iotMax)
    (construct-ugen "DustRange" audio-rate (list iotMin iotMax) nil 1 nil)))

(define exprandn
  (lambda (nc lo hi)
    (construct-ugen "ExpRandN" scalar-rate (list lo hi) nil nc nil)))

(define freezer
  (lambda (bufnum left right gain increment incrementOffset incrementRandom rightRandom syncPhaseTrigger randomizePhaseTrigger numberOfLoops)
    (construct-ugen "Freezer" audio-rate (list bufnum left right gain increment incrementOffset incrementRandom rightRandom syncPhaseTrigger randomizePhaseTrigger numberOfLoops) nil 1 nil)))

(define irandn
  (lambda (nc lo hi)
    (construct-ugen "IRandN" scalar-rate (list lo hi) nil nc nil)))

(define rlpfd
  (lambda (input ffreq res dist)
    (construct-ugen "RLPFD" (list 0) (list input ffreq res dist) nil 1 nil)))

(define rlagc
  (lambda (input timeUp curveUp timeDown curveDown)
    (construct-ugen "RLagC" (list 0) (list input timeUp curveUp timeDown curveDown) nil 1 nil)))

(define linrandn
  (lambda (nc lo hi minmax)
    (construct-ugen "LinRandN" scalar-rate (list lo hi minmax) nil nc nil)))

(define rloopset
  (lambda (rt bufnum left right gain increment spec)
    (construct-ugen "RLoopSet" rt (list bufnum left right gain increment spec) nil 1 nil)))

(define rmafoodchainl
  (lambda (rt freq a1 b1 d1 a2 b2 d2 k r h xi yi zi)
    (construct-ugen "RMAFoodChainL" rt (list freq a1 b1 d1 a2 b2 d2 k r h xi yi zi) nil 3 nil)))

(define rmeq
  (lambda (input freq rq k)
    (construct-ugen "RMEQ" (list 0) (list input freq rq k) nil 1 nil)))

(define rmeqsuite
  (lambda (rt maxSize)
    (construct-ugen "RMEQSuite" rt (list maxSize) nil 1 nil)))

(define rms
  (lambda (rt input lpFreq)
    (construct-ugen "RMS" rt (list input lpFreq) nil 1 nil)))

(define rmshelf
  (lambda (rt input freq k)
    (construct-ugen "RMShelf" rt (list input freq k) nil 1 nil)))

(define rmshelf2
  (lambda (rt input freq k)
    (construct-ugen "RMShelf2" rt (list input freq k) nil 1 nil)))

(define obxdfilter
  (lambda (input cutoff resonance multimode bandpass fourpole)
    (construct-ugen "ObxdFilter" (list 0) (list input cutoff resonance multimode bandpass fourpole) nil 1 nil)))

(define rpvdecaytbl
  (lambda (fft_buf decay_rate_buf history_buf)
    (construct-ugen "RPVDecayTbl" control-rate (list fft_buf decay_rate_buf history_buf) nil 1 nil)))

(define randn
  (lambda (nc lo hi)
    (construct-ugen "RandN" scalar-rate (list lo hi) nil nc nil)))

(define svfbp
  (lambda (rt input freq q)
    (construct-ugen "SvfBp" rt (list input freq q) nil 1 nil)))

(define svfhp
  (lambda (input freq q)
    (construct-ugen "SvfHp" (list 0) (list input freq q) nil 1 nil)))

(define svflp
  (lambda (rt input freq q)
    (construct-ugen "SvfLp" rt (list input freq q) nil 1 nil)))

(define shufflerb
  (lambda (bufnum readLocationMinima readLocationMaxima readIncrementMinima readIncrementMaxima durationMinima durationMaxima envelopeAmplitudeMinima envelopeAmplitudeMaxima envelopeShapeMinima envelopeShapeMaxima envelopeSkewMinima envelopeSkewMaxima stereoLocationMinima stereoLocationMaxima interOffsetTimeMinima interOffsetTimeMaxima ftableReadLocationIncrement readIncrementQuanta interOffsetTimeQuanta)
    (construct-ugen "ShufflerB" audio-rate (list bufnum readLocationMinima readLocationMaxima readIncrementMinima readIncrementMaxima durationMinima durationMaxima envelopeAmplitudeMinima envelopeAmplitudeMaxima envelopeShapeMinima envelopeShapeMaxima envelopeSkewMinima envelopeSkewMaxima stereoLocationMinima stereoLocationMaxima interOffsetTimeMinima interOffsetTimeMaxima ftableReadLocationIncrement readIncrementQuanta interOffsetTimeQuanta) nil 2 nil)))

(define rshufflerl
  (lambda (input fragmentSize maxDelay)
    (construct-ugen "RShufflerL" (list 0) (list input fragmentSize maxDelay) nil 1 nil)))

(define rsmplrindex
  (lambda (rt buf size mnn)
    (construct-ugen "RSmplrIndex" rt (list buf size mnn) nil 2 nil)))

(define texprandn
  (lambda (nc lo hi trigger)
    (construct-ugen "TExpRandN" (list 2) (list lo hi trigger) nil nc nil)))

(define tlinrandn
  (lambda (nc lo hi minmax trigger)
    (construct-ugen "TLinRandN" (list 3) (list lo hi minmax trigger) nil nc nil)))

(define trandn
  (lambda (nc lo hi trigger)
    (construct-ugen "TRandN" (list 2) (list lo hi trigger) nil nc nil)))

(define rtraceplay
  (lambda (rt bufnum degree rate_ axis)
    (construct-ugen "RTracePlay" rt (list bufnum degree rate_ axis) nil 1 nil)))

(define rtracerd
  (lambda (rt bufnum degree index axis)
    (construct-ugen "RTraceRd" rt (list bufnum degree index axis) nil 1 nil)))

(define reddpcmdecode
  (lambda (rt input)
    (construct-ugen "RedDPCMdecode" rt (list input) nil 1 nil)))

(define reddpcmencode
  (lambda (rt input round_)
    (construct-ugen "RedDPCMencode" rt (list input round_) nil 1 nil)))

(define redlbyl
  (lambda (rt input thresh samples)
    (construct-ugen "RedLbyl" rt (list input thresh samples) nil 1 nil)))

(define rednoise
  (lambda (rt clock)
    (construct-ugen "RedNoise" rt (list clock) nil 1 nil)))

(define redphasor
  (lambda (rt trig_ rate_ start end loop loopstart loopend)
    (construct-ugen "RedPhasor" rt (list trig_ rate_ start end loop loopstart loopend) nil 1 nil)))

(define redphasor2
  (lambda (rt trig_ rate_ start end loop loopstart loopend)
    (construct-ugen "RedPhasor2" rt (list trig_ rate_ start end loop loopstart loopend) nil 1 nil)))

(define regaliamitraeq
  (lambda (rt input freq rq k)
    (construct-ugen "RegaliaMitraEQ" rt (list input freq rq k) nil 1 nil)))

(define resonator
  (lambda (input freq position resolution structure brightness damping)
    (construct-ugen "Resonator" (list 0) (list input freq position resolution structure brightness damping) nil 1 nil)))

(define rongs
  (lambda (rt trigger sustain f0 structure brightness damping accent stretch position loss modeNum cosFreq)
    (construct-ugen "Rongs" rt (list trigger sustain f0 structure brightness damping accent stretch position loss modeNum cosFreq) nil 1 nil)))

(define rosslerl
  (lambda (rt freq a b c h xi yi zi)
    (construct-ugen "RosslerL" rt (list freq a b c h xi yi zi) nil 3 nil)))

(define rosslerresl
  (lambda (rt input stiff freq a b c h xi yi zi)
    (construct-ugen "RosslerResL" rt (list input stiff freq a b c h xi yi zi) nil 1 nil)))

(define rotate
  (lambda (rt w x y z rotate)
    (construct-ugen "Rotate" rt (list w x y z rotate) nil 1 nil)))

(define sid6581f
  (lambda (rt freqLo0 freqHi0 pwLo0 pwHi0 ctrl0 atkDcy0 susRel0 freqLo1 freqHi1 pwLo1 pwHi1 ctrl1 atkDcy1 susRel1 freqLo2 freqHi2 pwLo2 pwHi2 ctrl2 atkDcy2 susRel2 fcLo fcHi resFilt modeVol rate_)
    (construct-ugen "SID6581f" rt (list freqLo0 freqHi0 pwLo0 pwHi0 ctrl0 atkDcy0 susRel0 freqLo1 freqHi1 pwLo1 pwHi1 ctrl1 atkDcy1 susRel1 freqLo2 freqHi2 pwLo2 pwHi2 ctrl2 atkDcy2 susRel2 fcLo fcHi resFilt modeVol rate_) nil 1 nil)))

(define slonset
  (lambda (rt input memorysize1 before after threshold hysteresis)
    (construct-ugen "SLOnset" rt (list input memorysize1 before after threshold hysteresis) nil 1 nil)))

(define sms
  (lambda (input maxpeaks currentpeaks tolerance noisefloor freqmult freqadd formantpreserve useifft ampmult graphicsbufnum)
    (construct-ugen "SMS" (list 0) (list input maxpeaks currentpeaks tolerance noisefloor freqmult freqadd formantpreserve useifft ampmult graphicsbufnum) nil 2 nil)))

(define sn76489
  (lambda (rt tone0 tone1 tone2 noise vol0 vol1 vol2 vol3 rate_)
    (construct-ugen "SN76489" rt (list tone0 tone1 tone2 noise vol0 vol1 vol2 vol3 rate_) nil 1 nil)))

(define somareawr
  (lambda (rt bufnum inputdata coords netsize numdims nhood gate_)
    (construct-ugen "SOMAreaWr" rt (list bufnum inputdata coords netsize numdims nhood gate_) nil 1 nil)))

(define somrd
  (lambda (rt bufnum inputdata netsize numdims gate_)
    (construct-ugen "SOMRd" rt (list bufnum inputdata netsize numdims gate_) nil 2 nil)))

(define somtrain
  (lambda (rt bufnum inputdata netsize numdims traindur nhood gate_ initweight)
    (construct-ugen "SOMTrain" rt (list bufnum inputdata netsize numdims traindur nhood gate_ initweight) nil 3 nil)))

(define svf
  (lambda (signal cutoff res lowpass bandpass highpass notch peak)
    (construct-ugen "SVF" (list 0) (list signal cutoff res lowpass bandpass highpass notch peak) nil 1 nil)))

(define sawdpw
  (lambda (rt freq iphase)
    (construct-ugen "SawDPW" rt (list freq iphase) nil 1 nil)))

(define sensorydissonance
  (lambda (rt fft maxpeaks peakthreshold norm clamp)
    (construct-ugen "SensoryDissonance" rt (list fft maxpeaks peakthreshold norm clamp) nil 1 nil)))

(define sieve1
  (lambda (rt bufnum gap alternate)
    (construct-ugen "Sieve1" rt (list bufnum gap alternate) nil 1 nil)))

(define singrain
  (lambda (rt trigger dur freq)
    (construct-ugen "SinGrain" rt (list trigger dur freq) nil 1 nil)))

(define singrainb
  (lambda (rt trigger dur freq envbuf)
    (construct-ugen "SinGrainB" rt (list trigger dur freq envbuf) nil 1 nil)))

(define singrainbbf
  (lambda (rt trigger dur freq envbuf azimuth elevation rho wComp)
    (construct-ugen "SinGrainBBF" rt (list trigger dur freq envbuf azimuth elevation rho wComp) nil 4 nil)))

(define singrainbf
  (lambda (rt trigger dur freq azimuth elevation rho wComp)
    (construct-ugen "SinGrainBF" rt (list trigger dur freq azimuth elevation rho wComp) nil 4 nil)))

(define singraini
  (lambda (rt trigger dur freq envbuf1 envbuf2 ifac)
    (construct-ugen "SinGrainI" rt (list trigger dur freq envbuf1 envbuf2 ifac) nil 1 nil)))

(define singrainibf
  (lambda (rt trigger dur freq envbuf1 envbuf2 ifac azimuth elevation rho wComp)
    (construct-ugen "SinGrainIBF" rt (list trigger dur freq envbuf1 envbuf2 ifac azimuth elevation rho wComp) nil 4 nil)))

(define sintone
  (lambda (rt freq phase)
    (construct-ugen "SinTone" rt (list freq phase) nil 1 nil)))

(define sineshaper
  (lambda (input limit)
    (construct-ugen "SineShaper" (list 0) (list input limit) nil 1 nil)))

(define skipneedle
  (lambda (rt range rate_ offset)
    (construct-ugen "SkipNeedle" rt (list range rate_ offset) nil 1 nil)))

(define slide
  (lambda (rt input slideup slidedown)
    (construct-ugen "Slide" rt (list input slideup slidedown) nil 1 nil)))

(define slub
  (lambda (rt trig_ spike)
    (construct-ugen "Slub" rt (list trig_ spike) nil 1 nil)))

(define smoothdecimator
  (lambda (rt input rate_ smoothing)
    (construct-ugen "SmoothDecimator" rt (list input rate_ smoothing) nil 1 nil)))

(define softclipamp
  (lambda (input pregain)
    (construct-ugen "SoftClipAmp" (list 0) (list input pregain) nil 1 nil)))

(define softclipamp4
  (lambda (input pregain)
    (construct-ugen "SoftClipAmp4" (list 0) (list input pregain) nil 1 nil)))

(define softclipamp8
  (lambda (input pregain)
    (construct-ugen "SoftClipAmp8" (list 0) (list input pregain) nil 1 nil)))

(define softclipper4
  (lambda (rt input)
    (construct-ugen "SoftClipper4" rt (list input) nil 1 nil)))

(define softclipper8
  (lambda (rt input)
    (construct-ugen "SoftClipper8" rt (list input) nil 1 nil)))

(define sortbuf
  (lambda (rt bufnum sortrate reset)
    (construct-ugen "SortBuf" rt (list bufnum sortrate reset) nil 1 nil)))

(define spectralentropy
  (lambda (nc rt fft fftsize numbands)
    (construct-ugen "SpectralEntropy" rt (list fft fftsize numbands) nil nc nil)))

(define spreader
  (lambda (rt input theta filtsPerOctave)
    (construct-ugen "Spreader" rt (list input theta filtsPerOctave) nil 2 nil)))

(define sprucebudworm
  (lambda (rt reset rate_ k1 k2 alpha beta mu rho initx inity)
    (construct-ugen "SpruceBudworm" rt (list reset rate_ k1 k2 alpha beta mu rho initx inity) nil 2 nil)))

(define squiz
  (lambda (input pitchratio zcperchunk memlen)
    (construct-ugen "Squiz" (list 0) (list input pitchratio zcperchunk memlen) nil 1 nil)))

(define standard2dc
  (lambda (rt minfreq maxfreq k x0 y0)
    (construct-ugen "Standard2DC" rt (list minfreq maxfreq k x0 y0) nil 1 nil)))

(define standard2dl
  (lambda (rt minfreq maxfreq k x0 y0)
    (construct-ugen "Standard2DL" rt (list minfreq maxfreq k x0 y0) nil 1 nil)))

(define standard2dn
  (lambda (rt minfreq maxfreq k x0 y0)
    (construct-ugen "Standard2DN" rt (list minfreq maxfreq k x0 y0) nil 1 nil)))

(define standardtrig
  (lambda (rt minfreq maxfreq k x0 y0)
    (construct-ugen "StandardTrig" rt (list minfreq maxfreq k x0 y0) nil 1 nil)))

(define stkbandedwg
  (lambda (rt freq instr bowpressure bowmotion integration modalresonance bowvelocity setstriking trig_)
    (construct-ugen "StkBandedWG" rt (list freq instr bowpressure bowmotion integration modalresonance bowvelocity setstriking trig_) nil 1 nil)))

(define stkbeethree
  (lambda (rt freq op4gain op3gain lfospeed lfodepth adsrtarget trig_)
    (construct-ugen "StkBeeThree" rt (list freq op4gain op3gain lfospeed lfodepth adsrtarget trig_) nil 1 nil)))

(define stkblowhole
  (lambda (rt freq reedstiffness noisegain tonehole register breathpressure)
    (construct-ugen "StkBlowHole" rt (list freq reedstiffness noisegain tonehole register breathpressure) nil 1 nil)))

(define stkbowed
  (lambda (rt freq bowpressure bowposition vibfreq vibgain loudness_ gate_ attackrate decayrate)
    (construct-ugen "StkBowed" rt (list freq bowpressure bowposition vibfreq vibgain loudness_ gate_ attackrate decayrate) nil 1 nil)))

(define stkclarinet
  (lambda (rt freq reedstiffness noisegain vibfreq vibgain breathpressure trig_)
    (construct-ugen "StkClarinet" rt (list freq reedstiffness noisegain vibfreq vibgain breathpressure trig_) nil 1 nil)))

(define stkflute
  (lambda (rt freq jetDelay noisegain jetRatio)
    (construct-ugen "StkFlute" rt (list freq jetDelay noisegain jetRatio) nil 1 nil)))

(define stkglobals
  (lambda (rt showWarnings printErrors rawfilepath)
    (construct-ugen "StkGlobals" rt (list showWarnings printErrors rawfilepath) nil 1 nil)))

(define stkinst
  (lambda (rt freq gate_ onamp offamp instNumber args)
    (construct-ugen "StkInst" rt (list freq gate_ onamp offamp instNumber) args 1 nil)))

(define stkmandolin
  (lambda (rt freq bodysize pickposition stringdamping stringdetune aftertouch trig_)
    (construct-ugen "StkMandolin" rt (list freq bodysize pickposition stringdamping stringdetune aftertouch trig_) nil 1 nil)))

(define stkmodalbar
  (lambda (rt freq instrument stickhardness stickposition vibratogain vibratofreq directstickmix volume trig_)
    (construct-ugen "StkModalBar" rt (list freq instrument stickhardness stickposition vibratogain vibratofreq directstickmix volume trig_) nil 1 nil)))

(define stkmoog
  (lambda (rt freq filterQ sweeprate vibfreq vibgain gain trig_)
    (construct-ugen "StkMoog" rt (list freq filterQ sweeprate vibfreq vibgain gain trig_) nil 1 nil)))

(define stkpluck
  (lambda (rt freq decay_)
    (construct-ugen "StkPluck" rt (list freq decay_) nil 1 nil)))

(define stksaxofony
  (lambda (rt freq reedstiffness reedaperture noisegain blowposition vibratofrequency vibratogain breathpressure trig_)
    (construct-ugen "StkSaxofony" rt (list freq reedstiffness reedaperture noisegain blowposition vibratofrequency vibratogain breathpressure trig_) nil 1 nil)))

(define stkshakers
  (lambda (rt instr energy decay_ objects resfreq)
    (construct-ugen "StkShakers" rt (list instr energy decay_ objects resfreq) nil 1 nil)))

(define stkvoicform
  (lambda (rt freq vuvmix vowelphon vibfreq vibgain loudness_ trig_)
    (construct-ugen "StkVoicForm" rt (list freq vuvmix vowelphon vibfreq vibgain loudness_ trig_) nil 1 nil)))

(define streson
  (lambda (input delayTime res)
    (construct-ugen "Streson" (list 0) (list input delayTime res) nil 1 nil)))

(define stringvoice
  (lambda (rt trig_ infsustain freq accent structure brightness damping)
    (construct-ugen "StringVoice" rt (list trig_ infsustain freq accent structure brightness damping) nil 1 nil)))

(define summer
  (lambda (trig_ step reset resetval)
    (construct-ugen "Summer" (list 0) (list trig_ step reset resetval) nil 1 nil)))

(define switchdelay
  (lambda (input drylevel wetlevel delaytime delayfactor maxdelaytime)
    (construct-ugen "SwitchDelay" (list 0) (list input drylevel wetlevel delaytime delayfactor maxdelaytime) nil 1 nil)))

(define tbetarand
  (lambda (lo hi prob1 prob2 trig_)
    (construct-ugen "TBetaRand" (list 4) (list lo hi prob1 prob2 trig_) nil 1 nil)))

(define tbrownrand
  (lambda (lo hi dev dist trig_)
    (construct-ugen "TBrownRand" (list 4) (list lo hi dev dist trig_) nil 1 nil)))

(define tgaussrand
  (lambda (lo hi trig_)
    (construct-ugen "TGaussRand" (list 2) (list lo hi trig_) nil 1 nil)))

(define tgrains2
  (lambda (nc rt trigger bufnum rate_ centerPos dur pan amp att dec interp)
    (construct-ugen "TGrains2" rt (list trigger bufnum rate_ centerPos dur pan amp att dec interp) nil nc nil)))

(define tgrains3
  (lambda (nc rt trigger bufnum rate_ centerPos dur pan amp att dec window interp)
    (construct-ugen "TGrains3" rt (list trigger bufnum rate_ centerPos dur pan amp att dec window interp) nil nc nil)))

(define tpv
  (lambda (chain windowsize hopsize maxpeaks currentpeaks freqmult tolerance noisefloor)
    (construct-ugen "TPV" audio-rate (list chain windowsize hopsize maxpeaks currentpeaks freqmult tolerance noisefloor) nil 1 nil)))

(define tscramble
  (lambda (trigger inputs)
    (construct-ugen "TScramble" (list 0) (list trigger) inputs (length (mceChannels inputs)) nil)))

(define ttendency
  (lambda (rt trigger dist parX parY parA parB)
    (construct-ugen "TTendency" rt (list trigger dist parX parY parA parB) nil 1 nil)))

(define tartini
  (lambda (rt input threshold n k overlap smallCutoff)
    (construct-ugen "Tartini" rt (list input threshold n k overlap smallCutoff) nil 2 nil)))

(define termanwang
  (lambda (rt input reset ratex ratey alpha beta eta initx inity)
    (construct-ugen "TermanWang" rt (list input reset ratex ratey alpha beta eta initx inity) nil 1 nil)))

(define textvu
  (lambda (rt trig_ input label_ width reset ana)
    (construct-ugen "TextVU" rt (list trig_ input label_ width reset ana) nil 1 nil)))

(define tilt
  (lambda (rt w x y z tilt)
    (construct-ugen "Tilt" rt (list w x y z tilt) nil 1 nil)))

(define trigavg
  (lambda (rt input trig_)
    (construct-ugen "TrigAvg" rt (list input trig_) nil 1 nil)))

(define tumble
  (lambda (rt w x y z tilt)
    (construct-ugen "Tumble" rt (list w x y z tilt) nil 1 nil)))

(define twotube
  (lambda (rt input k loss d1length d2length)
    (construct-ugen "TwoTube" rt (list input k loss d1length d2length) nil 1 nil)))

(define uhj2b
  (lambda (rt ls rs)
    (construct-ugen "UHJ2B" rt (list ls rs) nil 3 nil)))

(define vbap
  (lambda (nc rt input bufnum azimuth elevation spread)
    (construct-ugen "VBAP" rt (list input bufnum azimuth elevation spread) nil nc nil)))

(define vbchebyfilt
  (lambda (rt input freq mode order)
    (construct-ugen "VBChebyFilt" rt (list input freq mode order) nil 1 nil)))

(define vbfourses
  (lambda (rt smoother freqarray)
    (construct-ugen "VBFourses" rt (list smoother) freqarray 4 nil)))

(define vbjonverb
  (lambda (input decay_ damping inputbw erfl tail_)
    (construct-ugen "VBJonVerb" (list 0) (list input decay_ damping inputbw erfl tail_) nil 2 nil)))

(define vbpvoc
  (lambda (rt numChannels bufnum playpos fftsize)
    (construct-ugen "VBPVoc" rt (list numChannels bufnum playpos fftsize) nil 1 nil)))

(define vmscan2d
  (lambda (rt bufnum)
    (construct-ugen "VMScan2D" rt (list bufnum) nil 2 nil)))

(define vosim
  (lambda (rt trig_ freq nCycles decay_)
    (construct-ugen "VOSIM" rt (list trig_ freq nCycles decay_) nil 1 nil)))

(define varshapeosc
  (lambda (rt freq pw waveshape sync syncfreq)
    (construct-ugen "VarShapeOsc" rt (list freq pw waveshape sync syncfreq) nil 1 nil)))

(define vosimosc
  (lambda (rt freq form1freq form2freq shape)
    (construct-ugen "VosimOsc" rt (list freq form1freq form2freq shape) nil 1 nil)))

(define wamp
  (lambda (rt input winSize)
    (construct-ugen "WAmp" rt (list input winSize) nil 1 nil)))

(define walshhadamard
  (lambda (rt input which)
    (construct-ugen "WalshHadamard" rt (list input which) nil 1 nil)))

(define warpz
  (lambda (nc rt bufnum pointer freqScale windowSize envbufnum overlaps windowRandRatio interp zeroSearch zeroStart)
    (construct-ugen "WarpZ" rt (list bufnum pointer freqScale windowSize envbufnum overlaps windowRandRatio interp zeroSearch zeroStart) nil nc nil)))

(define waveloss
  (lambda (rt input drop_ outof mode)
    (construct-ugen "WaveLoss" rt (list input drop_ outof mode) nil 1 nil)))

(define waveterrain
  (lambda (rt bufnum x y xsize ysize)
    (construct-ugen "WaveTerrain" rt (list bufnum x y xsize ysize) nil 1 nil)))

(define waveletdaub
  (lambda (rt input n which)
    (construct-ugen "WaveletDaub" rt (list input n which) nil 1 nil)))

(define weaklynonlinear
  (lambda (rt input reset ratex ratey freq initx inity alpha xexponent beta yexponent)
    (construct-ugen "WeaklyNonlinear" rt (list input reset ratex ratey freq initx inity alpha xexponent beta yexponent) nil 1 nil)))

(define weaklynonlinear2
  (lambda (rt input reset ratex ratey freq initx inity alpha xexponent beta yexponent)
    (construct-ugen "WeaklyNonlinear2" rt (list input reset ratex ratey freq initx inity alpha xexponent beta yexponent) nil 1 nil)))

(define werner
  (lambda (rt input freq damp feedback drive oversample)
    (construct-ugen "Werner" rt (list input freq damp feedback drive oversample) nil 1 nil)))

(define wrapsummer
  (lambda (rt trig_ step min_ max_ reset resetval)
    (construct-ugen "WrapSummer" rt (list trig_ step min_ max_ reset resetval) nil 1 nil)))

(define zosc
  (lambda (rt freq formantfreq shape mode)
    (construct-ugen "ZOsc" rt (list freq formantfreq shape mode) nil 1 nil)))

