(define binaryopugen
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 nil)))

(define unaryopugen
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 nil)))

(define a2k
  (lambda (input)
    (construct-ugen "A2K" control-rate (list input) nil 1 nil)))

(define apf
  (lambda (input freq radius)
    (construct-ugen "APF" (list 0) (list input freq radius) nil 1 nil)))

(define allpassc
  (lambda (input maxdelaytime delaytime decaytime)
    (construct-ugen "AllpassC" (list 0) (list input maxdelaytime delaytime decaytime) nil 1 nil)))

(define allpassl
  (lambda (input maxdelaytime delaytime decaytime)
    (construct-ugen "AllpassL" (list 0) (list input maxdelaytime delaytime decaytime) nil 1 nil)))

(define allpassn
  (lambda (input maxdelaytime delaytime decaytime)
    (construct-ugen "AllpassN" (list 0) (list input maxdelaytime delaytime decaytime) nil 1 nil)))

(define ampcomp
  (lambda (rt freq root exp_)
    (construct-ugen "AmpComp" rt (list freq root exp_) nil 1 nil)))

(define ampcompa
  (lambda (rt freq root minAmp rootAmp)
    (construct-ugen "AmpCompA" rt (list freq root minAmp rootAmp) nil 1 nil)))

(define amplitude
  (lambda (rt input attackTime releaseTime)
    (construct-ugen "Amplitude" rt (list input attackTime releaseTime) nil 1 nil)))

(define ballpass
  (lambda (input freq rq)
    (construct-ugen "BAllPass" (list 0) (list input freq rq) nil 1 nil)))

(define bbandpass
  (lambda (input freq bw)
    (construct-ugen "BBandPass" (list 0) (list input freq bw) nil 1 nil)))

(define bbandstop
  (lambda (input freq bw)
    (construct-ugen "BBandStop" (list 0) (list input freq bw) nil 1 nil)))

(define bhipass
  (lambda (input freq rq)
    (construct-ugen "BHiPass" (list 0) (list input freq rq) nil 1 nil)))

(define bhishelf
  (lambda (input freq rs db)
    (construct-ugen "BHiShelf" (list 0) (list input freq rs db) nil 1 nil)))

(define blowpass
  (lambda (input freq rq)
    (construct-ugen "BLowPass" (list 0) (list input freq rq) nil 1 nil)))

(define blowshelf
  (lambda (input freq rs db)
    (construct-ugen "BLowShelf" (list 0) (list input freq rs db) nil 1 nil)))

(define bpf
  (lambda (input freq rq)
    (construct-ugen "BPF" (list 0) (list input freq rq) nil 1 nil)))

(define bpz2
  (lambda (input)
    (construct-ugen "BPZ2" (list 0) (list input) nil 1 nil)))

(define bpeakeq
  (lambda (input freq rq db)
    (construct-ugen "BPeakEQ" (list 0) (list input freq rq db) nil 1 nil)))

(define brf
  (lambda (input freq rq)
    (construct-ugen "BRF" (list 0) (list input freq rq) nil 1 nil)))

(define brz2
  (lambda (input)
    (construct-ugen "BRZ2" (list 0) (list input) nil 1 nil)))

(define balance2
  (lambda (left right pos level)
    (construct-ugen "Balance2" (list 0 1) (list left right pos level) nil 2 nil)))

(define ball
  (lambda (rt input g damp friction)
    (construct-ugen "Ball" rt (list input g damp friction) nil 1 nil)))

(define beattrack
  (lambda (rt chain lock)
    (construct-ugen "BeatTrack" rt (list chain lock) nil 4 nil)))

(define beattrack2
  (lambda (rt busindex numfeatures windowsize phaseaccuracy lock weightingscheme)
    (construct-ugen "BeatTrack2" rt (list busindex numfeatures windowsize phaseaccuracy lock weightingscheme) nil 6 nil)))

(define bipanb2
  (lambda (rt inA inB azimuth gain)
    (construct-ugen "BiPanB2" rt (list inA inB azimuth gain) nil 3 nil)))

(define blip
  (lambda (rt freq numharm)
    (construct-ugen "Blip" rt (list freq numharm) nil 1 nil)))

(define blocksize (construct-ugen "BlockSize" scalar-rate nil nil 1 nil))

(define brownnoise
  (lambda (rt)
    (construct-ugen "BrownNoise" rt nil nil 1 nil)))

(define bufallpassc
  (lambda (buf input delaytime decaytime)
    (construct-ugen "BufAllpassC" (list 1) (list buf input delaytime decaytime) nil 1 nil)))

(define bufallpassl
  (lambda (buf input delaytime decaytime)
    (construct-ugen "BufAllpassL" (list 1) (list buf input delaytime decaytime) nil 1 nil)))

(define bufallpassn
  (lambda (buf input delaytime decaytime)
    (construct-ugen "BufAllpassN" (list 1) (list buf input delaytime decaytime) nil 1 nil)))

(define bufchannels
  (lambda (rt bufnum)
    (construct-ugen "BufChannels" rt (list bufnum) nil 1 nil)))

(define bufcombc
  (lambda (buf input delaytime decaytime)
    (construct-ugen "BufCombC" (list 1) (list buf input delaytime decaytime) nil 1 nil)))

(define bufcombl
  (lambda (buf input delaytime decaytime)
    (construct-ugen "BufCombL" (list 1) (list buf input delaytime decaytime) nil 1 nil)))

(define bufcombn
  (lambda (buf input delaytime decaytime)
    (construct-ugen "BufCombN" (list 1) (list buf input delaytime decaytime) nil 1 nil)))

(define bufdelayc
  (lambda (buf input delaytime)
    (construct-ugen "BufDelayC" (list 1) (list buf input delaytime) nil 1 nil)))

(define bufdelayl
  (lambda (buf input delaytime)
    (construct-ugen "BufDelayL" (list 1) (list buf input delaytime) nil 1 nil)))

(define bufdelayn
  (lambda (buf input delaytime)
    (construct-ugen "BufDelayN" (list 1) (list buf input delaytime) nil 1 nil)))

(define bufdur
  (lambda (rt bufnum)
    (construct-ugen "BufDur" rt (list bufnum) nil 1 nil)))

(define bufframes
  (lambda (rt bufnum)
    (construct-ugen "BufFrames" rt (list bufnum) nil 1 nil)))

(define bufratescale
  (lambda (rt bufnum)
    (construct-ugen "BufRateScale" rt (list bufnum) nil 1 nil)))

(define bufrd
  (lambda (nc rt bufnum phase loop interpolation)
    (construct-ugen "BufRd" rt (list bufnum phase loop interpolation) nil nc nil)))

(define bufsamplerate
  (lambda (rt bufnum)
    (construct-ugen "BufSampleRate" rt (list bufnum) nil 1 nil)))

(define bufsamples
  (lambda (rt bufnum)
    (construct-ugen "BufSamples" rt (list bufnum) nil 1 nil)))

(define bufwr
  (lambda (bufnum phase loop inputArray)
    (construct-ugen "BufWr" (list 3) (list bufnum phase loop) inputArray 1 nil)))

(define cosc
  (lambda (rt bufnum freq beats)
    (construct-ugen "COsc" rt (list bufnum freq beats) nil 1 nil)))

(define checkbadvalues
  (lambda (input id_ post)
    (construct-ugen "CheckBadValues" (list 0) (list input id_ post) nil 1 nil)))

(define clearbuf
  (lambda (rt buf)
    (construct-ugen "ClearBuf" rt (list buf) nil 1 nil)))

(define clip
  (lambda (input lo hi)
    (construct-ugen "Clip" (list 0) (list input lo hi) nil 1 nil)))

(define clipnoise
  (lambda (rt)
    (construct-ugen "ClipNoise" rt nil nil 1 nil)))

(define coingate
  (lambda (prob input)
    (construct-ugen "CoinGate" (list 1) (list prob input) nil 1 nil)))

(define combc
  (lambda (input maxdelaytime delaytime decaytime)
    (construct-ugen "CombC" (list 0) (list input maxdelaytime delaytime decaytime) nil 1 nil)))

(define combl
  (lambda (input maxdelaytime delaytime decaytime)
    (construct-ugen "CombL" (list 0) (list input maxdelaytime delaytime decaytime) nil 1 nil)))

(define combn
  (lambda (input maxdelaytime delaytime decaytime)
    (construct-ugen "CombN" (list 0) (list input maxdelaytime delaytime decaytime) nil 1 nil)))

(define compander
  (lambda (input control_ thresh slopeBelow slopeAbove clampTime relaxTime)
    (construct-ugen "Compander" (list 0) (list input control_ thresh slopeBelow slopeAbove clampTime relaxTime) nil 1 nil)))

(define companderd
  (lambda (rt input thresh slopeBelow slopeAbove clampTime relaxTime)
    (construct-ugen "CompanderD" rt (list input thresh slopeBelow slopeAbove clampTime relaxTime) nil 1 nil)))

(define controldur (construct-ugen "ControlDur" scalar-rate nil nil 1 nil))

(define controlrate (construct-ugen "ControlRate" scalar-rate nil nil 1 nil))

(define convolution
  (lambda (input kernel framesize)
    (construct-ugen "Convolution" audio-rate (list input kernel framesize) nil 1 nil)))

(define convolution2
  (lambda (input kernel trigger framesize)
    (construct-ugen "Convolution2" audio-rate (list input kernel trigger framesize) nil 1 nil)))

(define convolution2l
  (lambda (rt input kernel trigger framesize crossfade)
    (construct-ugen "Convolution2L" rt (list input kernel trigger framesize crossfade) nil 1 nil)))

(define convolution3
  (lambda (rt input kernel trigger framesize)
    (construct-ugen "Convolution3" rt (list input kernel trigger framesize) nil 1 nil)))

(define crackle
  (lambda (rt chaosParam)
    (construct-ugen "Crackle" rt (list chaosParam) nil 1 nil)))

(define cuspl
  (lambda (rt freq a b xi)
    (construct-ugen "CuspL" rt (list freq a b xi) nil 1 nil)))

(define cuspn
  (lambda (rt freq a b xi)
    (construct-ugen "CuspN" rt (list freq a b xi) nil 1 nil)))

(define dc
  (lambda (rt input)
    (construct-ugen "DC" rt (list input) nil 1 nil)))

(define dbrown
  (lambda (length_ lo hi step)
    (construct-ugen "Dbrown" demand-rate (list length_ lo hi step) nil 1 nil)))

(define dbufrd
  (lambda (bufnum phase loop)
    (construct-ugen "Dbufrd" demand-rate (list bufnum phase loop) nil 1 nil)))

(define dbufwr
  (lambda (bufnum phase input loop)
    (construct-ugen "Dbufwr" demand-rate (list bufnum phase input loop) nil 1 nil)))

(define dconst
  (lambda (sum_ input tolerance)
    (construct-ugen "Dconst" demand-rate (list sum_ input tolerance) nil 1 nil)))

(define decay
  (lambda (input decayTime)
    (construct-ugen "Decay" (list 0) (list input decayTime) nil 1 nil)))

(define decay2
  (lambda (input attackTime decayTime)
    (construct-ugen "Decay2" (list 0) (list input attackTime decayTime) nil 1 nil)))

(define decodeb2
  (lambda (nc w x y orientation)
    (construct-ugen "DecodeB2" (list 0 1 2) (list w x y orientation) nil nc nil)))

(define degreetokey
  (lambda (bufnum input octave)
    (construct-ugen "DegreeToKey" (list 1) (list bufnum input octave) nil 1 nil)))

(define deltaprd
  (lambda (buffer phase delTime interp)
    (construct-ugen "DelTapRd" (list 1) (list buffer phase delTime interp) nil 1 nil)))

(define deltapwr
  (lambda (buffer input)
    (construct-ugen "DelTapWr" (list 1) (list buffer input) nil 1 nil)))

(define delay1
  (lambda (input)
    (construct-ugen "Delay1" (list 0) (list input) nil 1 nil)))

(define delay2
  (lambda (input)
    (construct-ugen "Delay2" (list 0) (list input) nil 1 nil)))

(define delayc
  (lambda (input maxdelaytime delaytime)
    (construct-ugen "DelayC" (list 0) (list input maxdelaytime delaytime) nil 1 nil)))

(define delayl
  (lambda (input maxdelaytime delaytime)
    (construct-ugen "DelayL" (list 0) (list input maxdelaytime delaytime) nil 1 nil)))

(define delayn
  (lambda (input maxdelaytime delaytime)
    (construct-ugen "DelayN" (list 0) (list input maxdelaytime delaytime) nil 1 nil)))

(define demand
  (lambda (trig_ reset demandUGens)
    (construct-ugen "Demand" (list 0) (list trig_ reset) demandUGens (length (mceChannels demandUGens)) nil)))

(define demandenvgen
  (lambda (rt level dur shape curve gate_ reset levelScale levelBias timeScale doneAction)
    (construct-ugen "DemandEnvGen" rt (list level dur shape curve gate_ reset levelScale levelBias timeScale doneAction) nil 1 nil)))

(define detectindex
  (lambda (bufnum input)
    (construct-ugen "DetectIndex" (list 1) (list bufnum input) nil 1 nil)))

(define detectsilence
  (lambda (input amp time doneAction)
    (construct-ugen "DetectSilence" (list 0) (list input amp time doneAction) nil 1 nil)))

(define dgeom
  (lambda (length_ start grow)
    (construct-ugen "Dgeom" demand-rate (list length_ start grow) nil 1 nil)))

(define dibrown
  (lambda (length_ lo hi step)
    (construct-ugen "Dibrown" demand-rate (list length_ lo hi step) nil 1 nil)))

(define diskin
  (lambda (nc bufnum loop)
    (construct-ugen "DiskIn" audio-rate (list bufnum loop) nil nc nil)))

(define diskout
  (lambda (bufnum input)
    (construct-ugen "DiskOut" audio-rate (list bufnum) input 1 nil)))

(define diwhite
  (lambda (length_ lo hi)
    (construct-ugen "Diwhite" demand-rate (list length_ lo hi) nil 1 nil)))

(define done
  (lambda (src)
    (construct-ugen "Done" control-rate (list src) nil 1 nil)))

(define dpoll
  (lambda (input label_ run trigid)
    (construct-ugen "Dpoll" demand-rate (list input label_ run trigid) nil 1 nil)))

(define drand
  (lambda (repeats list_)
    (construct-ugen "Drand" demand-rate (list repeats) list_ 1 nil)))

(define dreset
  (lambda (input reset)
    (construct-ugen "Dreset" demand-rate (list input reset) nil 1 nil)))

(define dseq
  (lambda (repeats list_)
    (construct-ugen "Dseq" demand-rate (list repeats) list_ 1 nil)))

(define dser
  (lambda (repeats list_)
    (construct-ugen "Dser" demand-rate (list repeats) list_ 1 nil)))

(define dseries
  (lambda (length_ start step)
    (construct-ugen "Dseries" demand-rate (list length_ start step) nil 1 nil)))

(define dshuf
  (lambda (repeats list_)
    (construct-ugen "Dshuf" demand-rate (list repeats) list_ 1 nil)))

(define dstutter
  (lambda (n input)
    (construct-ugen "Dstutter" demand-rate (list n input) nil 1 nil)))

(define dswitch
  (lambda (index list_)
    (construct-ugen "Dswitch" demand-rate (list index) list_ 1 nil)))

(define dswitch1
  (lambda (index list_)
    (construct-ugen "Dswitch1" demand-rate (list index) list_ 1 nil)))

(define dunique
  (lambda (source maxBufferSize protected)
    (construct-ugen "Dunique" demand-rate (list source maxBufferSize protected) nil 1 nil)))

(define dust
  (lambda (rt density)
    (construct-ugen "Dust" rt (list density) nil 1 nil)))

(define dust2
  (lambda (rt density)
    (construct-ugen "Dust2" rt (list density) nil 1 nil)))

(define duty
  (lambda (rt dur reset doneAction level)
    (construct-ugen "Duty" rt (list dur reset doneAction level) nil 1 nil)))

(define dwhite
  (lambda (length_ lo hi)
    (construct-ugen "Dwhite" demand-rate (list length_ lo hi) nil 1 nil)))

(define dwrand
  (lambda (repeats weights list_)
    (construct-ugen "Dwrand" demand-rate (list repeats weights) list_ 1 nil)))

(define dxrand
  (lambda (repeats list_)
    (construct-ugen "Dxrand" demand-rate (list repeats) list_ 1 nil)))

(define envgen
  (lambda (rt gate_ levelScale levelBias timeScale doneAction envelope_)
    (construct-ugen "EnvGen" rt (list gate_ levelScale levelBias timeScale doneAction) envelope_ 1 nil)))

(define exprand
  (lambda (lo hi)
    (construct-ugen "ExpRand" scalar-rate (list lo hi) nil 1 nil)))

(define fbsinec
  (lambda (rt freq im fb a c xi yi)
    (construct-ugen "FBSineC" rt (list freq im fb a c xi yi) nil 1 nil)))

(define fbsinel
  (lambda (rt freq im fb a c xi yi)
    (construct-ugen "FBSineL" rt (list freq im fb a c xi yi) nil 1 nil)))

(define fbsinen
  (lambda (rt freq im fb a c xi yi)
    (construct-ugen "FBSineN" rt (list freq im fb a c xi yi) nil 1 nil)))

(define fft
  (lambda (buffer input hop wintype active winsize)
    (construct-ugen "FFT" control-rate (list buffer input hop wintype active winsize) nil 1 nil)))

(define fos
  (lambda (input a0 a1 b1)
    (construct-ugen "FOS" (list 0) (list input a0 a1 b1) nil 1 nil)))

(define fsinosc
  (lambda (rt freq iphase)
    (construct-ugen "FSinOsc" rt (list freq iphase) nil 1 nil)))

(define fold
  (lambda (input lo hi)
    (construct-ugen "Fold" (list 0) (list input lo hi) nil 1 nil)))

(define formant
  (lambda (rt fundfreq formfreq bwfreq)
    (construct-ugen "Formant" rt (list fundfreq formfreq bwfreq) nil 1 nil)))

(define formlet
  (lambda (input freq attacktime decaytime)
    (construct-ugen "Formlet" (list 0) (list input freq attacktime decaytime) nil 1 nil)))

(define free
  (lambda (trig_ id_)
    (construct-ugen "Free" (list 0) (list trig_ id_) nil 1 nil)))

(define freeself
  (lambda (input)
    (construct-ugen "FreeSelf" control-rate (list input) nil 1 nil)))

(define freeselfwhendone
  (lambda (src)
    (construct-ugen "FreeSelfWhenDone" control-rate (list src) nil 1 nil)))

(define freeverb
  (lambda (input mix room damp)
    (construct-ugen "FreeVerb" (list 0) (list input mix room damp) nil 1 nil)))

(define freeverb2
  (lambda (input in2 mix room damp)
    (construct-ugen "FreeVerb2" (list 0) (list input in2 mix room damp) nil 2 nil)))

(define freqshift
  (lambda (input freq phase)
    (construct-ugen "FreqShift" audio-rate (list input freq phase) nil 1 nil)))

(define gverb
  (lambda (input roomsize revtime damping inputbw spread drylevel earlyreflevel taillevel maxroomsize)
    (construct-ugen "GVerb" (list 0) (list input roomsize revtime damping inputbw spread drylevel earlyreflevel taillevel maxroomsize) nil 2 nil)))

(define gate
  (lambda (input trig_)
    (construct-ugen "Gate" (list 0) (list input trig_) nil 1 nil)))

(define gbmanl
  (lambda (rt freq xi yi)
    (construct-ugen "GbmanL" rt (list freq xi yi) nil 1 nil)))

(define gbmann
  (lambda (rt freq xi yi)
    (construct-ugen "GbmanN" rt (list freq xi yi) nil 1 nil)))

(define gendy1
  (lambda (rt ampdist durdist adparam ddparam minfreq maxfreq ampscale durscale initCPs knum)
    (construct-ugen "Gendy1" rt (list ampdist durdist adparam ddparam minfreq maxfreq ampscale durscale initCPs knum) nil 1 nil)))

(define gendy2
  (lambda (rt ampdist durdist adparam ddparam minfreq maxfreq ampscale durscale initCPs knum a c)
    (construct-ugen "Gendy2" rt (list ampdist durdist adparam ddparam minfreq maxfreq ampscale durscale initCPs knum a c) nil 1 nil)))

(define gendy3
  (lambda (rt ampdist durdist adparam ddparam freq ampscale durscale initCPs knum)
    (construct-ugen "Gendy3" rt (list ampdist durdist adparam ddparam freq ampscale durscale initCPs knum) nil 1 nil)))

(define grainbuf
  (lambda (nc trigger dur sndbuf rate_ pos interp pan envbufnum maxGrains)
    (construct-ugen "GrainBuf" audio-rate (list trigger dur sndbuf rate_ pos interp pan envbufnum maxGrains) nil nc nil)))

(define grainfm
  (lambda (nc trigger dur carfreq modfreq index pan envbufnum maxGrains)
    (construct-ugen "GrainFM" audio-rate (list trigger dur carfreq modfreq index pan envbufnum maxGrains) nil nc nil)))

(define grainin
  (lambda (nc trigger dur input pan envbufnum maxGrains)
    (construct-ugen "GrainIn" audio-rate (list trigger dur input pan envbufnum maxGrains) nil nc nil)))

(define grainsin
  (lambda (nc trigger dur freq pan envbufnum maxGrains)
    (construct-ugen "GrainSin" audio-rate (list trigger dur freq pan envbufnum maxGrains) nil nc nil)))

(define graynoise
  (lambda (rt)
    (construct-ugen "GrayNoise" rt nil nil 1 nil)))

(define hpf
  (lambda (input freq)
    (construct-ugen "HPF" (list 0) (list input freq) nil 1 nil)))

(define hpz1
  (lambda (input)
    (construct-ugen "HPZ1" (list 0) (list input) nil 1 nil)))

(define hpz2
  (lambda (input)
    (construct-ugen "HPZ2" (list 0) (list input) nil 1 nil)))

(define hasher
  (lambda (input)
    (construct-ugen "Hasher" (list 0) (list input) nil 1 nil)))

(define henonc
  (lambda (rt freq a b x0 x1)
    (construct-ugen "HenonC" rt (list freq a b x0 x1) nil 1 nil)))

(define henonl
  (lambda (rt freq a b x0 x1)
    (construct-ugen "HenonL" rt (list freq a b x0 x1) nil 1 nil)))

(define henonn
  (lambda (rt freq a b x0 x1)
    (construct-ugen "HenonN" rt (list freq a b x0 x1) nil 1 nil)))

(define hilbert
  (lambda (input)
    (construct-ugen "Hilbert" (list 0) (list input) nil 2 nil)))

(define ienvgen
  (lambda (rt index envelope_)
    (construct-ugen "IEnvGen" rt (list index) envelope_ 1 nil)))

(define ifft
  (lambda (buffer wintype winsize)
    (construct-ugen "IFFT" audio-rate (list buffer wintype winsize) nil 1 nil)))

(define irand
  (lambda (lo hi)
    (construct-ugen "IRand" scalar-rate (list lo hi) nil 1 nil)))

(define impulse
  (lambda (rt freq phase)
    (construct-ugen "Impulse" rt (list freq phase) nil 1 nil)))

(define in
  (lambda (nc rt bus)
    (construct-ugen "In" rt (list bus) nil nc nil)))

(define infeedback
  (lambda (nc bus)
    (construct-ugen "InFeedback" audio-rate (list bus) nil nc nil)))

(define inrange
  (lambda (input lo hi)
    (construct-ugen "InRange" (list 0) (list input lo hi) nil 1 nil)))

(define inrect
  (lambda (rt x y rect)
    (construct-ugen "InRect" rt (list x y rect) nil 1 nil)))

(define intrig
  (lambda (nc bus)
    (construct-ugen "InTrig" control-rate (list bus) nil nc nil)))

(define index
  (lambda (bufnum input)
    (construct-ugen "Index" (list 1) (list bufnum input) nil 1 nil)))

(define indexinbetween
  (lambda (bufnum input)
    (construct-ugen "IndexInBetween" (list 1) (list bufnum input) nil 1 nil)))

(define indexl
  (lambda (bufnum input)
    (construct-ugen "IndexL" (list 1) (list bufnum input) nil 1 nil)))

(define infougenbase
  (lambda (rt)
    (construct-ugen "InfoUGenBase" rt nil nil 1 nil)))

(define integrator
  (lambda (input coef)
    (construct-ugen "Integrator" (list 0) (list input coef) nil 1 nil)))

(define k2a
  (lambda (input)
    (construct-ugen "K2A" audio-rate (list input) nil 1 nil)))

(define keystate
  (lambda (rt keycode minval maxval lag)
    (construct-ugen "KeyState" rt (list keycode minval maxval lag) nil 1 nil)))

(define keytrack
  (lambda (rt chain keydecay chromaleak)
    (construct-ugen "KeyTrack" rt (list chain keydecay chromaleak) nil 1 nil)))

(define klang
  (lambda (rt freqscale freqoffset specificationsArrayRef)
    (construct-ugen "Klang" rt (list freqscale freqoffset) specificationsArrayRef 1 nil)))

(define klank
  (lambda (input freqscale freqoffset decayscale specificationsArrayRef)
    (construct-ugen "Klank" (list 0) (list input freqscale freqoffset decayscale) specificationsArrayRef 1 nil)))

(define lfclipnoise
  (lambda (rt freq)
    (construct-ugen "LFClipNoise" rt (list freq) nil 1 nil)))

(define lfcub
  (lambda (rt freq iphase)
    (construct-ugen "LFCub" rt (list freq iphase) nil 1 nil)))

(define lfdclipnoise
  (lambda (rt freq)
    (construct-ugen "LFDClipNoise" rt (list freq) nil 1 nil)))

(define lfdnoise0
  (lambda (rt freq)
    (construct-ugen "LFDNoise0" rt (list freq) nil 1 nil)))

(define lfdnoise1
  (lambda (rt freq)
    (construct-ugen "LFDNoise1" rt (list freq) nil 1 nil)))

(define lfdnoise3
  (lambda (rt freq)
    (construct-ugen "LFDNoise3" rt (list freq) nil 1 nil)))

(define lfgauss
  (lambda (rt duration width iphase loop doneAction)
    (construct-ugen "LFGauss" rt (list duration width iphase loop doneAction) nil 1 nil)))

(define lfnoise0
  (lambda (rt freq)
    (construct-ugen "LFNoise0" rt (list freq) nil 1 nil)))

(define lfnoise1
  (lambda (rt freq)
    (construct-ugen "LFNoise1" rt (list freq) nil 1 nil)))

(define lfnoise2
  (lambda (rt freq)
    (construct-ugen "LFNoise2" rt (list freq) nil 1 nil)))

(define lfpar
  (lambda (rt freq iphase)
    (construct-ugen "LFPar" rt (list freq iphase) nil 1 nil)))

(define lfpulse
  (lambda (rt freq iphase width)
    (construct-ugen "LFPulse" rt (list freq iphase width) nil 1 nil)))

(define lfsaw
  (lambda (rt freq iphase)
    (construct-ugen "LFSaw" rt (list freq iphase) nil 1 nil)))

(define lftri
  (lambda (rt freq iphase)
    (construct-ugen "LFTri" rt (list freq iphase) nil 1 nil)))

(define lpf
  (lambda (input freq)
    (construct-ugen "LPF" (list 0) (list input freq) nil 1 nil)))

(define lpz1
  (lambda (input)
    (construct-ugen "LPZ1" (list 0) (list input) nil 1 nil)))

(define lpz2
  (lambda (input)
    (construct-ugen "LPZ2" (list 0) (list input) nil 1 nil)))

(define lag
  (lambda (input lagTime)
    (construct-ugen "Lag" (list 0) (list input lagTime) nil 1 nil)))

(define lag2
  (lambda (input lagTime)
    (construct-ugen "Lag2" (list 0) (list input lagTime) nil 1 nil)))

(define lag2ud
  (lambda (input lagTimeU lagTimeD)
    (construct-ugen "Lag2UD" (list 0) (list input lagTimeU lagTimeD) nil 1 nil)))

(define lag3
  (lambda (input lagTime)
    (construct-ugen "Lag3" (list 0) (list input lagTime) nil 1 nil)))

(define lag3ud
  (lambda (input lagTimeU lagTimeD)
    (construct-ugen "Lag3UD" (list 0) (list input lagTimeU lagTimeD) nil 1 nil)))

(define lagin
  (lambda (nc bus lag)
    (construct-ugen "LagIn" control-rate (list bus lag) nil nc nil)))

(define lagud
  (lambda (input lagTimeU lagTimeD)
    (construct-ugen "LagUD" (list 0) (list input lagTimeU lagTimeD) nil 1 nil)))

(define lastvalue
  (lambda (input diff)
    (construct-ugen "LastValue" (list 0) (list input diff) nil 1 nil)))

(define latch
  (lambda (input trig_)
    (construct-ugen "Latch" (list 0) (list input trig_) nil 1 nil)))

(define latoocarfianc
  (lambda (rt freq a b c d xi yi)
    (construct-ugen "LatoocarfianC" rt (list freq a b c d xi yi) nil 1 nil)))

(define latoocarfianl
  (lambda (rt freq a b c d xi yi)
    (construct-ugen "LatoocarfianL" rt (list freq a b c d xi yi) nil 1 nil)))

(define latoocarfiann
  (lambda (rt freq a b c d xi yi)
    (construct-ugen "LatoocarfianN" rt (list freq a b c d xi yi) nil 1 nil)))

(define leakdc
  (lambda (input coef)
    (construct-ugen "LeakDC" (list 0) (list input coef) nil 1 nil)))

(define leastchange
  (lambda (rt a b)
    (construct-ugen "LeastChange" rt (list a b) nil 1 nil)))

(define limiter
  (lambda (input level dur)
    (construct-ugen "Limiter" (list 0) (list input level dur) nil 1 nil)))

(define lincongc
  (lambda (rt freq a c m xi)
    (construct-ugen "LinCongC" rt (list freq a c m xi) nil 1 nil)))

(define lincongl
  (lambda (rt freq a c m xi)
    (construct-ugen "LinCongL" rt (list freq a c m xi) nil 1 nil)))

(define lincongn
  (lambda (rt freq a c m xi)
    (construct-ugen "LinCongN" rt (list freq a c m xi) nil 1 nil)))

(define linexp
  (lambda (input srclo srchi dstlo dsthi)
    (construct-ugen "LinExp" (list 0) (list input srclo srchi dstlo dsthi) nil 1 nil)))

(define linpan2
  (lambda (input pos level)
    (construct-ugen "LinPan2" (list 0) (list input pos level) nil 2 nil)))

(define linrand
  (lambda (lo hi minmax)
    (construct-ugen "LinRand" scalar-rate (list lo hi minmax) nil 1 nil)))

(define linxfade2
  (lambda (inA inB pan)
    (construct-ugen "LinXFade2" (list 0 1) (list inA inB pan) nil 1 nil)))

(define line
  (lambda (rt start end dur doneAction)
    (construct-ugen "Line" rt (list start end dur doneAction) nil 1 nil)))

(define linen
  (lambda (gate_ attackTime susLevel releaseTime doneAction)
    (construct-ugen "Linen" control-rate (list gate_ attackTime susLevel releaseTime doneAction) nil 1 nil)))

(define localbuf
  (lambda (numChannels numFrames)
    (construct-ugen "LocalBuf" scalar-rate (list numChannels numFrames) nil 1 nil)))

(define localin
  (lambda (nc rt default_)
    (construct-ugen "LocalIn" rt nil default_ nc nil)))

(define localout
  (lambda (input)
    (construct-ugen "LocalOut" (list 0) nil input 0 nil)))

(define logistic
  (lambda (rt chaosParam freq init_)
    (construct-ugen "Logistic" rt (list chaosParam freq init_) nil 1 nil)))

(define lorenzl
  (lambda (rt freq s r b h xi yi zi)
    (construct-ugen "LorenzL" rt (list freq s r b h xi yi zi) nil 1 nil)))

(define loudness
  (lambda (chain smask tmask)
    (construct-ugen "Loudness" control-rate (list chain smask tmask) nil 1 nil)))

(define mfcc
  (lambda (rt chain numcoeff)
    (construct-ugen "MFCC" rt (list chain numcoeff) nil 13 nil)))

(define mantissamask
  (lambda (input bits)
    (construct-ugen "MantissaMask" (list 0) (list input bits) nil 1 nil)))

(define median
  (lambda (length_ input)
    (construct-ugen "Median" (list 1) (list length_ input) nil 1 nil)))

(define mideq
  (lambda (input freq rq db)
    (construct-ugen "MidEQ" (list 0) (list input freq rq db) nil 1 nil)))

(define moddif
  (lambda (x y mod_)
    (construct-ugen "ModDif" (list 0) (list x y mod_) nil 1 nil)))

(define moogff
  (lambda (input freq gain reset)
    (construct-ugen "MoogFF" (list 0) (list input freq gain reset) nil 1 nil)))

(define mostchange
  (lambda (a b)
    (construct-ugen "MostChange" (list 0 1) (list a b) nil 1 nil)))

(define mousebutton
  (lambda (rt minval maxval lag)
    (construct-ugen "MouseButton" rt (list minval maxval lag) nil 1 nil)))

(define mousex
  (lambda (rt minval maxval warp lag)
    (construct-ugen "MouseX" rt (list minval maxval warp lag) nil 1 nil)))

(define mousey
  (lambda (rt minval maxval warp lag)
    (construct-ugen "MouseY" rt (list minval maxval warp lag) nil 1 nil)))

(define nrand
  (lambda (lo hi n)
    (construct-ugen "NRand" scalar-rate (list lo hi n) nil 1 nil)))

(define nodeid
  (lambda (rt)
    (construct-ugen "NodeID" rt nil nil 1 nil)))

(define normalizer
  (lambda (input level dur)
    (construct-ugen "Normalizer" (list 0) (list input level dur) nil 1 nil)))

(define numaudiobuses (construct-ugen "NumAudioBuses" scalar-rate nil nil 1 nil))

(define numbuffers (construct-ugen "NumBuffers" scalar-rate nil nil 1 nil))

(define numcontrolbuses (construct-ugen "NumControlBuses" scalar-rate nil nil 1 nil))

(define numinputbuses (construct-ugen "NumInputBuses" scalar-rate nil nil 1 nil))

(define numoutputbuses (construct-ugen "NumOutputBuses" scalar-rate nil nil 1 nil))

(define numrunningsynths (construct-ugen "NumRunningSynths" scalar-rate nil nil 1 nil))

(define offsetout
  (lambda (bus input)
    (construct-ugen "OffsetOut" (list 1) (list bus) input 0 nil)))

(define onepole
  (lambda (input coef)
    (construct-ugen "OnePole" (list 0) (list input coef) nil 1 nil)))

(define onezero
  (lambda (input coef)
    (construct-ugen "OneZero" (list 0) (list input coef) nil 1 nil)))

(define onsets
  (lambda (chain threshold odftype relaxtime floor_ mingap medianspan whtype rawodf)
    (construct-ugen "Onsets" control-rate (list chain threshold odftype relaxtime floor_ mingap medianspan whtype rawodf) nil 1 nil)))

(define osc
  (lambda (rt bufnum freq phase)
    (construct-ugen "Osc" rt (list bufnum freq phase) nil 1 nil)))

(define oscn
  (lambda (rt bufnum freq phase)
    (construct-ugen "OscN" rt (list bufnum freq phase) nil 1 nil)))

(define out
  (lambda (bus input)
    (construct-ugen "Out" (list 1) (list bus) input 0 nil)))

(define psingrain
  (lambda (rt freq dur amp)
    (construct-ugen "PSinGrain" rt (list freq dur amp) nil 1 nil)))

(define pv_add
  (lambda (bufferA bufferB)
    (construct-ugen "PV_Add" control-rate (list bufferA bufferB) nil 1 nil)))

(define pv_binscramble
  (lambda (buffer wipe width trig_)
    (construct-ugen "PV_BinScramble" control-rate (list buffer wipe width trig_) nil 1 nil)))

(define pv_binshift
  (lambda (buffer stretch shift interp)
    (construct-ugen "PV_BinShift" control-rate (list buffer stretch shift interp) nil 1 nil)))

(define pv_binwipe
  (lambda (bufferA bufferB wipe)
    (construct-ugen "PV_BinWipe" control-rate (list bufferA bufferB wipe) nil 1 nil)))

(define pv_brickwall
  (lambda (buffer wipe)
    (construct-ugen "PV_BrickWall" control-rate (list buffer wipe) nil 1 nil)))

(define pv_conformalmap
  (lambda (buffer areal aimag)
    (construct-ugen "PV_ConformalMap" control-rate (list buffer areal aimag) nil 1 nil)))

(define pv_conj
  (lambda (buffer)
    (construct-ugen "PV_Conj" control-rate (list buffer) nil 1 nil)))

(define pv_copy
  (lambda (bufferA bufferB)
    (construct-ugen "PV_Copy" control-rate (list bufferA bufferB) nil 1 nil)))

(define pv_copyphase
  (lambda (bufferA bufferB)
    (construct-ugen "PV_CopyPhase" control-rate (list bufferA bufferB) nil 1 nil)))

(define pv_diffuser
  (lambda (buffer trig_)
    (construct-ugen "PV_Diffuser" control-rate (list buffer trig_) nil 1 nil)))

(define pv_div
  (lambda (bufferA bufferB)
    (construct-ugen "PV_Div" control-rate (list bufferA bufferB) nil 1 nil)))

(define pv_hainsworthfoote
  (lambda (maxSize)
    (construct-ugen "PV_HainsworthFoote" control-rate (list maxSize) nil 1 nil)))

(define pv_jensenandersen
  (lambda (maxSize)
    (construct-ugen "PV_JensenAndersen" control-rate (list maxSize) nil 1 nil)))

(define pv_localmax
  (lambda (buffer threshold)
    (construct-ugen "PV_LocalMax" control-rate (list buffer threshold) nil 1 nil)))

(define pv_magabove
  (lambda (buffer threshold)
    (construct-ugen "PV_MagAbove" control-rate (list buffer threshold) nil 1 nil)))

(define pv_magbelow
  (lambda (buffer threshold)
    (construct-ugen "PV_MagBelow" control-rate (list buffer threshold) nil 1 nil)))

(define pv_magclip
  (lambda (buffer threshold)
    (construct-ugen "PV_MagClip" control-rate (list buffer threshold) nil 1 nil)))

(define pv_magdiv
  (lambda (bufferA bufferB zeroed)
    (construct-ugen "PV_MagDiv" control-rate (list bufferA bufferB zeroed) nil 1 nil)))

(define pv_magfreeze
  (lambda (buffer freeze)
    (construct-ugen "PV_MagFreeze" control-rate (list buffer freeze) nil 1 nil)))

(define pv_magmul
  (lambda (bufferA bufferB)
    (construct-ugen "PV_MagMul" control-rate (list bufferA bufferB) nil 1 nil)))

(define pv_magnoise
  (lambda (buffer)
    (construct-ugen "PV_MagNoise" control-rate (list buffer) nil 1 nil)))

(define pv_magshift
  (lambda (buffer stretch shift)
    (construct-ugen "PV_MagShift" control-rate (list buffer stretch shift) nil 1 nil)))

(define pv_magsmear
  (lambda (buffer bins)
    (construct-ugen "PV_MagSmear" control-rate (list buffer bins) nil 1 nil)))

(define pv_magsquared
  (lambda (buffer)
    (construct-ugen "PV_MagSquared" control-rate (list buffer) nil 1 nil)))

(define pv_max
  (lambda (bufferA bufferB)
    (construct-ugen "PV_Max" control-rate (list bufferA bufferB) nil 1 nil)))

(define pv_min
  (lambda (bufferA bufferB)
    (construct-ugen "PV_Min" control-rate (list bufferA bufferB) nil 1 nil)))

(define pv_mul
  (lambda (bufferA bufferB)
    (construct-ugen "PV_Mul" control-rate (list bufferA bufferB) nil 1 nil)))

(define pv_phaseshift
  (lambda (buffer shift integrate)
    (construct-ugen "PV_PhaseShift" control-rate (list buffer shift integrate) nil 1 nil)))

(define pv_phaseshift270
  (lambda (buffer)
    (construct-ugen "PV_PhaseShift270" control-rate (list buffer) nil 1 nil)))

(define pv_phaseshift90
  (lambda (buffer)
    (construct-ugen "PV_PhaseShift90" control-rate (list buffer) nil 1 nil)))

(define pv_randcomb
  (lambda (buffer wipe trig_)
    (construct-ugen "PV_RandComb" control-rate (list buffer wipe trig_) nil 1 nil)))

(define pv_randwipe
  (lambda (bufferA bufferB wipe trig_)
    (construct-ugen "PV_RandWipe" control-rate (list bufferA bufferB wipe trig_) nil 1 nil)))

(define pv_rectcomb
  (lambda (buffer numTeeth phase width)
    (construct-ugen "PV_RectComb" control-rate (list buffer numTeeth phase width) nil 1 nil)))

(define pv_rectcomb2
  (lambda (bufferA bufferB numTeeth phase width)
    (construct-ugen "PV_RectComb2" control-rate (list bufferA bufferB numTeeth phase width) nil 1 nil)))

(define pan2
  (lambda (input pos level)
    (construct-ugen "Pan2" (list 0) (list input pos level) nil 2 nil)))

(define pan4
  (lambda (rt input xpos ypos level)
    (construct-ugen "Pan4" rt (list input xpos ypos level) nil 4 nil)))

(define panaz
  (lambda (nc input pos level width orientation)
    (construct-ugen "PanAz" (list 0) (list input pos level width orientation) nil nc nil)))

(define panb
  (lambda (rt input azimuth elevation gain)
    (construct-ugen "PanB" rt (list input azimuth elevation gain) nil 4 nil)))

(define panb2
  (lambda (input azimuth gain)
    (construct-ugen "PanB2" (list 0) (list input azimuth gain) nil 3 nil)))

(define partconv
  (lambda (input fftsize irbufnum)
    (construct-ugen "PartConv" audio-rate (list input fftsize irbufnum) nil 1 nil)))

(define pause
  (lambda (gate_ id_)
    (construct-ugen "Pause" control-rate (list gate_ id_) nil 1 nil)))

(define pauseself
  (lambda (input)
    (construct-ugen "PauseSelf" control-rate (list input) nil 1 nil)))

(define pauseselfwhendone
  (lambda (src)
    (construct-ugen "PauseSelfWhenDone" control-rate (list src) nil 1 nil)))

(define peak
  (lambda (input trig_)
    (construct-ugen "Peak" (list 0) (list input trig_) nil 1 nil)))

(define peakfollower
  (lambda (input decay_)
    (construct-ugen "PeakFollower" (list 0) (list input decay_) nil 1 nil)))

(define phasor
  (lambda (rt trig_ rate_ start end resetPos)
    (construct-ugen "Phasor" rt (list trig_ rate_ start end resetPos) nil 1 nil)))

(define pinknoise
  (lambda (rt)
    (construct-ugen "PinkNoise" rt nil nil 1 nil)))

(define pitch
  (lambda (input initFreq minFreq maxFreq execFreq maxBinsPerOctave median ampThreshold peakThreshold downSample clar)
    (construct-ugen "Pitch" control-rate (list input initFreq minFreq maxFreq execFreq maxBinsPerOctave median ampThreshold peakThreshold downSample clar) nil 2 nil)))

(define pitchshift
  (lambda (input windowSize pitchRatio pitchDispersion timeDispersion)
    (construct-ugen "PitchShift" (list 0) (list input windowSize pitchRatio pitchDispersion timeDispersion) nil 1 nil)))

(define playbuf
  (lambda (nc rt bufnum rate_ trigger startPos loop doneAction)
    (construct-ugen "PlayBuf" rt (list bufnum rate_ trigger startPos loop doneAction) nil nc nil)))

(define pluck
  (lambda (input trig_ maxdelaytime delaytime decaytime coef)
    (construct-ugen "Pluck" (list 0) (list input trig_ maxdelaytime delaytime decaytime coef) nil 1 nil)))

(define poll
  (lambda (trig_ input trigid label_)
    (construct-ugen "Poll" (list 1) (list trig_ input trigid label_) nil 1 nil)))

(define pulse
  (lambda (rt freq width)
    (construct-ugen "Pulse" rt (list freq width) nil 1 nil)))

(define pulsecount
  (lambda (trig_ reset)
    (construct-ugen "PulseCount" (list 0) (list trig_ reset) nil 1 nil)))

(define pulsedivider
  (lambda (trig_ div_ start)
    (construct-ugen "PulseDivider" (list 0) (list trig_ div_ start) nil 1 nil)))

(define quadc
  (lambda (rt freq a b c xi)
    (construct-ugen "QuadC" rt (list freq a b c xi) nil 1 nil)))

(define quadl
  (lambda (rt freq a b c xi)
    (construct-ugen "QuadL" rt (list freq a b c xi) nil 1 nil)))

(define quadn
  (lambda (rt freq a b c xi)
    (construct-ugen "QuadN" rt (list freq a b c xi) nil 1 nil)))

(define rhpf
  (lambda (input freq rq)
    (construct-ugen "RHPF" (list 0) (list input freq rq) nil 1 nil)))

(define rlpf
  (lambda (input freq rq)
    (construct-ugen "RLPF" (list 0) (list input freq rq) nil 1 nil)))

(define radianspersample (construct-ugen "RadiansPerSample" scalar-rate nil nil 1 nil))

(define ramp
  (lambda (input lagTime)
    (construct-ugen "Ramp" (list 0) (list input lagTime) nil 1 nil)))

(define rand
  (lambda (lo hi)
    (construct-ugen "Rand" scalar-rate (list lo hi) nil 1 nil)))

(define randid
  (lambda (rt id_)
    (construct-ugen "RandID" rt (list id_) nil 0 nil)))

(define randseed
  (lambda (rt trig_ seed)
    (construct-ugen "RandSeed" rt (list trig_ seed) nil 0 nil)))

(define recordbuf
  (lambda (rt bufnum offset recLevel preLevel run loop trigger doneAction inputArray)
    (construct-ugen "RecordBuf" rt (list bufnum offset recLevel preLevel run loop trigger doneAction) inputArray 1 nil)))

(define replaceout
  (lambda (bus input)
    (construct-ugen "ReplaceOut" (list 1) (list bus) input 0 nil)))

(define resonz
  (lambda (input freq bwr)
    (construct-ugen "Resonz" (list 0) (list input freq bwr) nil 1 nil)))

(define ringz
  (lambda (input freq decaytime)
    (construct-ugen "Ringz" (list 0) (list input freq decaytime) nil 1 nil)))

(define rotate2
  (lambda (x y pos)
    (construct-ugen "Rotate2" (list 0 1) (list x y pos) nil 2 nil)))

(define runningmax
  (lambda (input trig_)
    (construct-ugen "RunningMax" (list 0) (list input trig_) nil 1 nil)))

(define runningmin
  (lambda (input trig_)
    (construct-ugen "RunningMin" (list 0) (list input trig_) nil 1 nil)))

(define runningsum
  (lambda (input numsamp)
    (construct-ugen "RunningSum" (list 0) (list input numsamp) nil 1 nil)))

(define sos
  (lambda (input a0 a1 a2 b1 b2)
    (construct-ugen "SOS" (list 0) (list input a0 a1 a2 b1 b2) nil 1 nil)))

(define sampledur (construct-ugen "SampleDur" scalar-rate nil nil 1 nil))

(define samplerate (construct-ugen "SampleRate" scalar-rate nil nil 1 nil))

(define sanitize
  (lambda (input replace)
    (construct-ugen "Sanitize" (list 0) (list input replace) nil 1 nil)))

(define saw
  (lambda (rt freq)
    (construct-ugen "Saw" rt (list freq) nil 1 nil)))

(define schmidt
  (lambda (input lo hi)
    (construct-ugen "Schmidt" (list 0) (list input lo hi) nil 1 nil)))

(define select
  (lambda (which array)
    (construct-ugen "Select" (list 0 1) (list which) array 1 nil)))

(define sendtrig
  (lambda (input id_ value)
    (construct-ugen "SendTrig" (list 0) (list input id_ value) nil 0 nil)))

(define setresetff
  (lambda (trig_ reset)
    (construct-ugen "SetResetFF" (list 0 1) (list trig_ reset) nil 1 nil)))

(define shaper
  (lambda (bufnum input)
    (construct-ugen "Shaper" (list 1) (list bufnum input) nil 1 nil)))

(define sinosc
  (lambda (rt freq phase)
    (construct-ugen "SinOsc" rt (list freq phase) nil 1 nil)))

(define sinoscfb
  (lambda (rt freq feedback)
    (construct-ugen "SinOscFB" rt (list freq feedback) nil 1 nil)))

(define slew
  (lambda (input up dn)
    (construct-ugen "Slew" (list 0) (list input up dn) nil 1 nil)))

(define slope
  (lambda (input)
    (construct-ugen "Slope" (list 0) (list input) nil 1 nil)))

(define speccentroid
  (lambda (rt buffer)
    (construct-ugen "SpecCentroid" rt (list buffer) nil 1 nil)))

(define specflatness
  (lambda (rt buffer)
    (construct-ugen "SpecFlatness" rt (list buffer) nil 1 nil)))

(define specpcile
  (lambda (rt buffer fraction interpolate)
    (construct-ugen "SpecPcile" rt (list buffer fraction interpolate) nil 1 nil)))

(define spring
  (lambda (rt input spring damp)
    (construct-ugen "Spring" rt (list input spring damp) nil 1 nil)))

(define standardl
  (lambda (rt freq k xi yi)
    (construct-ugen "StandardL" rt (list freq k xi yi) nil 1 nil)))

(define standardn
  (lambda (rt freq k xi yi)
    (construct-ugen "StandardN" rt (list freq k xi yi) nil 1 nil)))

(define stepper
  (lambda (trig_ reset min_ max_ step resetval)
    (construct-ugen "Stepper" (list 0) (list trig_ reset min_ max_ step resetval) nil 1 nil)))

(define stereoconvolution2l
  (lambda (rt input kernelL kernelR trigger framesize crossfade)
    (construct-ugen "StereoConvolution2L" rt (list input kernelL kernelR trigger framesize crossfade) nil 2 nil)))

(define subsampleoffset (construct-ugen "SubsampleOffset" scalar-rate nil nil 1 nil))

(define sum3
  (lambda (in0 in1 in2)
    (construct-ugen "Sum3" (list 0 1 2) (list in0 in1 in2) nil 1 nil)))

(define sum4
  (lambda (in0 in1 in2 in3)
    (construct-ugen "Sum4" (list 0 1 2 3) (list in0 in1 in2 in3) nil 1 nil)))

(define sweep
  (lambda (trig_ rate_)
    (construct-ugen "Sweep" (list 0) (list trig_ rate_) nil 1 nil)))

(define syncsaw
  (lambda (rt syncFreq sawFreq)
    (construct-ugen "SyncSaw" rt (list syncFreq sawFreq) nil 1 nil)))

(define t2a
  (lambda (input offset)
    (construct-ugen "T2A" audio-rate (list input offset) nil 1 nil)))

(define t2k
  (lambda (input)
    (construct-ugen "T2K" control-rate (list input) nil 1 nil)))

(define tball
  (lambda (rt input g damp friction)
    (construct-ugen "TBall" rt (list input g damp friction) nil 1 nil)))

(define tdelay
  (lambda (input dur)
    (construct-ugen "TDelay" (list 0) (list input dur) nil 1 nil)))

(define tduty
  (lambda (rt dur reset doneAction level gapFirst)
    (construct-ugen "TDuty" rt (list dur reset doneAction level gapFirst) nil 1 nil)))

(define texprand
  (lambda (lo hi trig_)
    (construct-ugen "TExpRand" (list 2) (list lo hi trig_) nil 1 nil)))

(define tgrains
  (lambda (nc trigger bufnum rate_ centerPos dur pan amp interp)
    (construct-ugen "TGrains" audio-rate (list trigger bufnum rate_ centerPos dur pan amp interp) nil nc nil)))

(define tirand
  (lambda (lo hi trig_)
    (construct-ugen "TIRand" (list 2) (list lo hi trig_) nil 1 nil)))

(define trand
  (lambda (lo hi trig_)
    (construct-ugen "TRand" (list 2) (list lo hi trig_) nil 1 nil)))

(define twindex
  (lambda (input normalize array)
    (construct-ugen "TWindex" (list 0) (list input normalize) array 1 nil)))

(define timer
  (lambda (trig_)
    (construct-ugen "Timer" (list 0) (list trig_) nil 1 nil)))

(define toggleff
  (lambda (trig_)
    (construct-ugen "ToggleFF" (list 0) (list trig_) nil 1 nil)))

(define trig
  (lambda (input dur)
    (construct-ugen "Trig" (list 0) (list input dur) nil 1 nil)))

(define trig1
  (lambda (input dur)
    (construct-ugen "Trig1" (list 0) (list input dur) nil 1 nil)))

(define twopole
  (lambda (input freq radius)
    (construct-ugen "TwoPole" (list 0) (list input freq radius) nil 1 nil)))

(define twozero
  (lambda (input freq radius)
    (construct-ugen "TwoZero" (list 0) (list input freq radius) nil 1 nil)))

(define vdiskin
  (lambda (nc bufnum rate_ loop sendID)
    (construct-ugen "VDiskIn" audio-rate (list bufnum rate_ loop sendID) nil nc nil)))

(define vosc
  (lambda (rt bufpos freq phase)
    (construct-ugen "VOsc" rt (list bufpos freq phase) nil 1 nil)))

(define vosc3
  (lambda (rt bufpos freq1 freq2 freq3)
    (construct-ugen "VOsc3" rt (list bufpos freq1 freq2 freq3) nil 1 nil)))

(define varlag
  (lambda (input time curvature warp start)
    (construct-ugen "VarLag" (list 0) (list input time curvature warp start) nil 1 nil)))

(define varsaw
  (lambda (rt freq iphase width)
    (construct-ugen "VarSaw" rt (list freq iphase width) nil 1 nil)))

(define vibrato
  (lambda (rt freq rate_ depth delay onset rateVariation depthVariation iphase trig_)
    (construct-ugen "Vibrato" rt (list freq rate_ depth delay onset rateVariation depthVariation iphase trig_) nil 1 nil)))

(define warp1
  (lambda (nc bufnum pointer freqScale windowSize envbufnum overlaps windowRandRatio interp)
    (construct-ugen "Warp1" audio-rate (list bufnum pointer freqScale windowSize envbufnum overlaps windowRandRatio interp) nil nc nil)))

(define whitenoise
  (lambda (rt)
    (construct-ugen "WhiteNoise" rt nil nil 1 nil)))

(define wrap
  (lambda (input lo hi)
    (construct-ugen "Wrap" (list 0) (list input lo hi) nil 1 nil)))

(define wrapindex
  (lambda (bufnum input)
    (construct-ugen "WrapIndex" (list 1) (list bufnum input) nil 1 nil)))

(define xfade2
  (lambda (inA inB pan level)
    (construct-ugen "XFade2" (list 0 1) (list inA inB pan level) nil 1 nil)))

(define xline
  (lambda (rt start end dur doneAction)
    (construct-ugen "XLine" rt (list start end dur doneAction) nil 1 nil)))

(define xout
  (lambda (bus xfade input)
    (construct-ugen "XOut" (list 2) (list bus xfade) input 0 nil)))

(define zerocrossing
  (lambda (input)
    (construct-ugen "ZeroCrossing" (list 0) (list input) nil 1 nil)))

(define maxlocalbufs
  (lambda (count)
    (construct-ugen "MaxLocalBufs" control-rate (list count) nil 1 nil)))

(define muladd
  (lambda (input mul add)
    (construct-ugen "MulAdd" (list 0 1 2) (list input mul add) nil 1 nil)))

(define setbuf
  (lambda (buf offset length_ array)
    (construct-ugen "SetBuf" scalar-rate (list buf offset length_) array 1 nil)))

(define neg
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 0)))

(define u:not
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 1)))

(define isnil
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 2)))

(define notnil
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 3)))

(define bitnot
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 4)))

(define u:abs
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 5)))

(define asfloat
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 6)))

(define asint
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 7)))

(define ceil
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 8)))

(define u:floor
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 9)))

(define frac
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 10)))

(define sign
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 11)))

(define squared
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 12)))

(define cubed
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 13)))

(define u:sqrt
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 14)))

(define u:exp
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 15)))

(define recip
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 16)))

(define midicps
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 17)))

(define cpsmidi
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 18)))

(define midiratio
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 19)))

(define ratiomidi
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 20)))

(define dbamp
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 21)))

(define ampdb
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 22)))

(define octcps
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 23)))

(define cpsoct
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 24)))

(define u:log
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 25)))

(define log2
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 26)))

(define log10
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 27)))

(define u:sin
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 28)))

(define u:cos
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 29)))

(define u:tan
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 30)))

(define arcsin
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 31)))

(define arccos
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 32)))

(define arctan
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 33)))

(define sinh
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 34)))

(define cosh
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 35)))

(define tanh
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 36)))

(define rand_
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 37)))

(define rand2
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 38)))

(define linrand_
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 39)))

(define bilinrand
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 40)))

(define sum3rand
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 41)))

(define distort
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 42)))

(define softclip
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 43)))

(define coin
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 44)))

(define digitvalue
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 45)))

(define silence
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 46)))

(define thru
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 47)))

(define rectwindow
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 48)))

(define hanwindow
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 49)))

(define welchwindow
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 50)))

(define triwindow
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 51)))

(define ramp_
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 52)))

(define scurve
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 53)))

(define add
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 0)))

(define sub
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 1)))

(define mul
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 2)))

(define idiv
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 3)))

(define fdiv
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 4)))

(define u:mod
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 5)))

(define eq
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 6)))

(define ne
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 7)))

(define lt
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 8)))

(define gt
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 9)))

(define le
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 10)))

(define ge
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 11)))

(define u:min
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 12)))

(define u:max
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 13)))

(define bitand
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 14)))

(define bitor
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 15)))

(define bitxor
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 16)))

(define u:lcm
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 17)))

(define u:gcd
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 18)))

(define u:round
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 19)))

(define roundup
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 20)))

(define trunc
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 21)))

(define atan2
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 22)))

(define hypot
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 23)))

(define hypotx
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 24)))

(define pow
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 25)))

(define shiftleft
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 26)))

(define shiftright
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 27)))

(define unsignedshift
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 28)))

(define fill
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 29)))

(define ring1
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 30)))

(define ring2
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 31)))

(define ring3
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 32)))

(define ring4
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 33)))

(define difsqr
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 34)))

(define sumsqr
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 35)))

(define sqrsum
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 36)))

(define sqrdif
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 37)))

(define absdif
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 38)))

(define thresh
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 39)))

(define amclip
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 40)))

(define scaleneg
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 41)))

(define clip2
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 42)))

(define excess
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 43)))

(define fold2
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 44)))

(define wrap2
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 45)))

(define firstarg
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 46)))

(define randrange
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 47)))

(define exprandrange
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 48)))

