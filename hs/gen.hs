import Data.Char {- base -}

import qualified Sound.Sc3.Ugen.Db as Db {- hsc3-db -}
import qualified Sound.Sc3.Ugen.Db.Bindings.Scheme as Scheme {- hsc3-db -}
import qualified Sound.Sc3.Ugen.Db.Record as Db {- hsc3-db -}
import qualified Sound.Sc3.Ugen.Db.Rename as Rename {- hsc3-db -}

gen_ugens :: (String -> String) -> IO ()
gen_ugens f = do
  let c = map (\nm -> f nm) (Db.complete_names Db.ugen_db_core)
      x = map (\nm -> f nm) (map Db.ugen_name Db.ugen_db_ext)
  writeFile "/home/rohan/sw/rsc3-arf/src/db/ugen-core.scm" (unlines c)
  writeFile "/home/rohan/sw/rsc3-arf/src/db/ugen-ext.scm" (unlines x)

gen_explicit_rate_ugens :: IO ()
gen_explicit_rate_ugens = gen_ugens (Scheme.scheme_mk_ugen (Rename.scheme_rename . map Data.Char.toLower))

main :: IO ()
main = gen_explicit_rate_ugens
