rsc3-arf
--------

rsc3 alternate rate flow.

In rsc3-arf Ugens are marked as generators or filters.  Generators require
an operating rate input (unless they are specialised and only operate
at one rate) and filters derive their rate from their input.  In a
sense rate information flows down the signal graph.

In _rsc3_ unit generator constructors assign _default_
operating rates to all Ugens, selecting in each case the highest
allowable rate.

The function _kr_ traverses a Ugen graph and re-writes all audio rate
Ugens to control rate.  Initialisation and demand rate nodes are not
edited.

The function _ar_ inserts an _end-kr_ marker into the graph to halt
_kr_ traversal.  This allows, for instance, a control rate subgraph to
end after an _Amplitude_ Ugen, which then reads from an audio rate
signal.

Names of [rsc3-arf](http://rohandrape.net/?t=rsc3-arf) unit generators
are given the same names as in SuperCollider but lower-cased
(ie. sinosc or u:abs) in place of the names at
[rsc3](http://rohandrape.net/?t=rsc3) (ie. SinOsc or Abs).  This makes
the libraries compatible.

The graph:

    (let* ((o (muladd (lfsaw kr: (mce2 8 7.23) 0) 3 80))
           (f (muladd (lfsaw kr: 0.4 0) 24 o))
           (s (mul (sinosc ar: (midicps f) 0) 0.1)))
      (combn s 0.2 0.2 4))

is equivalent to the _rsc3_ graph:

    (let* ((o (MulAdd (LFSaw (Mce2 8 7.23) 0) 3 80))
           (f (MulAdd (LFSaw 0.4 0) 24 o))
           (s (Mul (SinOsc (kr (MidiCps f)) 0) 0.1)))
      (CombN s 0.2 0.2 4))

The graph:

    (let* ((a (amplitude kr: (soundin 0) 0.01 0.01))
           (f (muladd a 1200 400)))
      (mul (sinosc ar: f 0) 0.1))

is equivalent to the _rsc3_ graph:

    (let* ((a (kr (Amplitude (ar (SoundIn 0)) 0.01 0.01)))
           (f (MulAdd a 1200 400)))
      (Mul (SinOsc f 0) 0.1))

Initially the model here was the model at _rsc3_, and vice-versa.

© [rohan drape](http://rohandrape.net), 2005-2022, [gpl](http://gnu.org/copyleft/)
