; 20060920 (rd)
(mul
	(add
		(formlet
			(blip ar 10 12)
			(muladd (lfnoise0 ar '(20 40)) 43 700)
			0.005
			(mousex kr 0.012 0.19 1 0.1))
		(mul
			(sinosc ar 40 0)
			(lfnoise0 ar '(5 10))))
	(line kr 0 0.25 2.5 donothing))
