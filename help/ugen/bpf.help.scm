; bpf
(let* ((t (impulse kr (muladd (lfnoise0 kr '(3 3.25)) 16 18) 0))
       (e (decay2 t 0.01 (trand 0.005 (mousey kr 0.01 0.15 0 0.1) t))))
  (bpf
   (mul (whitenoise ar) e)
   (trand 10 (mousex kr 100 12000 0 0.1) t)
   (trand 0.0 1.0 t)))
