-- stkModalBar ; event control
let f g _x y z o rx _ry p _px _py =
      let tr = g - 0.5
          freq = MidiCps p
          instr = 4
          sig = StkModalBar freq instr ((1 - y) * 127) (y * 127) 64 64 (rx * 127) 127 tr
      in Pan2 sig (o * 2 - 1) (Latch z g * 3)
in Mix (Voicer 16 f)
