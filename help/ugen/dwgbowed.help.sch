-- dwgBowed ; event control
let f g _x y z o rx ry p _px _py =
      let s1 = dwgbowed (MidiCps p) z (0.5 + rx) 1 y 0.1 0.25 31 (0.55 + ry) 2 * g * z
          s2 = dwgsoundboard s1 20 20 0.8 199 211 223 227 229 233 239 241
          s3 = bpf s2 118 1 + s2
          s4 = bpf s3 430 1 + s3
          s5 = bpf s4 490 1 + s4
          s6 = lpf s5 6000
      in Pan2 s6 (o * 2 - 1) 1
in Mix (Voicer 16 f) * 0.25
