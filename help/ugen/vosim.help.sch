-- Vosim ; event control
let f g _x y z o rx _ry p _px _py =
      let trg = Impulse (MidiCps p) 0
          frq = LinExp y 0 1 440 880
      in Pan2 (VOSIM trg frq 1 rx) (o * 2 - 1) (g * z)
in Mix (Voicer 16 f)
