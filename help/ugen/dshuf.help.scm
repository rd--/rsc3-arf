; dshuf
(let* ((trig (impulse kr 11 0))
       (freq (demand trig 0 (dseq dinf (dshuf 2 '(110 220 440 880))))))
  (mul (sinosc ar freq 0) 0.1))

; dshuf
(let* ((freq (lambda (rt) (demand (impulse kr rt 0) 0 (dseq dinf (dshuf 2 '(110 220 440 880)))))))
  (mul (sinosc ar (map freq '(5 11)) 0) 0.1))
