; tirand
(let* ((t (impulse ar (mousex kr 1 32 0 0.1) 0))
       (n (tirand 16 72 t)))
  (mul3
   (sinosc ar (midicps (add n 36)) 0)
   (decay2 t 0.01 0.5)
   0.1))
