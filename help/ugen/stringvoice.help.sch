-- stringVoice ; event control
let f g x y z o rx ry _p _px _py =
      let freq = MidiCps (x * 25 + 36)
          tr = Trig g (ControlDur ())
          sig = StringVoice tr 0 freq z y rx (ry * 2)
      in Pan2 sig (o * 2 - 1) 1
in Mix (Voicer 16 f)
