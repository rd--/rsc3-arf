; formlet
(let*
    ((f
      (lambda (t g j n f)
	(let ((pd (pulsedivider t j 0)))
	  (mul
	   (formlet
	    pd
	    (mul f (tirand '(2 1) n pd))
	    (trand 0.01 0.04 pd)
	    (trand 0.05 0.10 pd))
	   g))))
     (n
      (lambda (t)
	(list
	 (f t 0.15 2 9 '(200 400))
	 (f t 0.25 2 9 (list (add 200 (trand 0 1 t)) (add 400 (trand 0 1 t))))
	 (f t 0.05 4 5 '(25 50))
	 (f t 0.15 4 5 (list (add 25 (trand 0 1 t)) (add 50 (trand 0 1 t))))
	 (mul
	  (f t 0.5 1 16 '(300 600))
	  (latch (coingate 0.2 t) t))))))
  (mul
   (muladd (lfnoise0 kr 2) 0.25 0.25)
   (foldl1 add (n (impulse ar 24 0)))))
