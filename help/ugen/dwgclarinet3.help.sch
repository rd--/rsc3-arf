-- dwgclarinet3 ; event control
let f g x y z o rx ry _p _px _py =
      let freq = MidiCps (x * 25 + 42)
          vib = SinOsc (y * 4) 0 * (z * 0.005) + 1
          amp = LinLin z 0 1 0.65 1
          pm = amp * g
          pc = LinLin y 0 1 0.25 0.85
          m = LinLin rx 0 1 0.4 1.25
          c1 = ry
          c3 = LinLin o 0 1 5 9
          signal = dwgclarinet3 (freq * krate vib) (k2a pm) pc m 1 0.01 c1 c3
      in Pan2 (hpf signal 200) (o * 2 - 1) 1
in Mix (Voicer 16 f) * 0.25
