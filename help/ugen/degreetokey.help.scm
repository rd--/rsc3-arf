; degreetokey
(let* ((t (dust kr 9))
       (b (tchoose t '(36 48 60 72)))
       (n (mul (lfnoise1 kr '(3 3.05)) 0.04))
       (d (tirand (mousex kr 15 0 0 0.1) (mousey kr 15 27 0 0.1) t))
       (k (degreetokey (aslocalbuf 1 '(0 2 3.2 5 7 9 10)) d 12))
       (e (decay2 t 0.005 (trand 0.02 0.15 t)))
       (m (mul3 e (sinosc ar (midicps (add3 b k n)) 0) 0.2))
       (u (pulsedivider t 9 0)))
  (add
   (mul m 0.75)
   (allpassc m 0.15 (trand 0.0075 0.125 u) (trand 0.05 0.15 u))))
