-- membraneHexagon ; event control
let f g x y z o rx _ry _p _px _py =
      let ex = PinkNoise () * EnvGen g z 0 0.1 doNothing (envPerc (0.02 * rx) 1 1 [-4, -4])
          tn = x * 0.1
          ls = LinExp y 0 1 0.999999 0.9999
      in Pan2 (MembraneHexagon ex tn ls) (o * 2 - 1) 1
in Mix (Voicer 8 f)
