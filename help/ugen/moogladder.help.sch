-- MoogLadder ; event control
let f g _x y z o rx ry p _px _py =
      let f0 = UnitCps p
          f1 = f0 * (1 + y * 8)
          res = rx + ry
          env = LagUD g 0.05 (2 - y * 2) * (2 - y) * z
      in Pan2 (MoogLadder (LFSaw f0 0) f1 res) (o * 2 - 1) env
in Mix (Voicer 16 f)
