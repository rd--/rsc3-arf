; drand
(let* ((trig (impulse kr 11 0))
       (freq (demand trig 0 (drand dinf '(440 880 220)))))
  (mul (sinosc ar freq 0) 0.1))

; drand
(let* ((freq (lambda (rt) (demand (impulse kr rt 0) 0 (drand dinf '(440 880 220))))))
  (mul (sinosc ar (map freq '(5 7)) 0) 0.1))
