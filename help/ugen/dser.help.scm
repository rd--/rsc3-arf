; dser
(let* ((freq (demand (impulse kr 11 0) 0 (dser 15 '(440 880 660 1760)))))
  (mul (sinosc ar freq 0) 0.1))

; dser
(let* ((freq (lambda (rt) (demand (impulse kr rt 0) 0 (dser 49 '(440 880 660 1760))))))
  (splay2 (mul (sinosc ar (map freq '(5 7 11 13 17)) 0) 0.1)))
