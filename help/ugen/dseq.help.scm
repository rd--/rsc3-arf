; dseq
(let* ((trig (impulse kr 11 0))
       (freq (demand trig 0 (dseq dinf '(440 880 220)))))
  (mul (sinosc ar freq 0) 0.1))

; dseq
(let* ((freq (lambda (rt) (demand (impulse kr rt 0) 0 (dseq dinf '(440 880 220))))))
  (splay2 (mul (sinosc ar (map freq '(3 5 7 11 17)) 0) 0.1)))
