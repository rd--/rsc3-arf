; dxrand
(let* ((trig (impulse kr 11 0))
       (freq (demand trig 0 (dxrand dinf '(440 880 220)))))
  (mul (sinosc ar freq 0) 0.1))

; dxrand
(let* ((freq (lambda (rt) (demand (impulse kr rt 0) 0 (dxrand dinf '(440 880 220))))))
  (splay2 (mul (sinosc ar (map freq '(7 11 13)) 0) 0.1)))
