; r6rs
(import (rsc3 core) (rsc3 arf))

(Mul (SinOsc 440 0) 0.1)

(Mul (SinOsc (MouseX 40 400 0 0.1) 0) 0.1)

(Mul (SinOsc (kr: (MouseX 40 400 0 0.1)) 0) 0.1)

(Mul (SinOsc (mce2 (kr: (MouseX 432 439 0 0.1)) 440) 0) 0.15)
