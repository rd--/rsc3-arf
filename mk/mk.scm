(import (rnrs) (mk-r6rs core))

(define rsc3-arf-file-seq (list "db/ugen-core.scm" "db/ugen-ext.scm" "arf.scm"))

(define at-src
  (lambda (x)
    (string-append "../src/" x)))

(mk-r6rs '(rsc3 arf)
	 (map at-src rsc3-arf-file-seq)
	 (string-append (list-ref (command-line) 1) "/rsc3/arf.sls")
	 '((rnrs) (rhs core) (sosc core) (rsc3 core) (rsc3 server) (rsc3 ugen) (rsc3 lang))
	 '()
	 '())

(exit)
