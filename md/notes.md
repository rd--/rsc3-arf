The _rsc3-arf_ project implemented a rate flow model that has since been adopted by _stsc3_ and _jssc3_.

It is now the model at _rsc3_ and the model formerly at _rsc3_ is now here.

At present _rsc3_ and _rsc3-arf_ uses case rules to distinguish between the two rate models.

_rsc3_ has _SinOsc_ where _rsc3-arf_ has _sinosc_.

While the latter name can be derived from the former without a lookup table, the reverse is not possible.

(In the past _rsc3_ had _sin-osc_, however that naming convention does not work with _.sch_ notation, where it would mean _sin - osc_.)

(The naming convention of _hsc3_ can distinguish _sinOsc_ but not _line_.)

There are example graphs at _rsc3-arf_ which would need to be case edited to work if the case rules were changed.
